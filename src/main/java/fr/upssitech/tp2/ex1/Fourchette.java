package fr.upssitech.tp2.ex1;

public class Fourchette {

  private boolean isTake = false;
  private int numero;

  public Fourchette (int numero){
    this.numero = numero;
  }

  public synchronized void prendre() {
    while (this.isTake) {
      try {
        this.wait();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    this.isTake = true;
  }

  public synchronized void déposer() {
    this.isTake = false;
    this.notify();
  }

  @Override
  public String toString() {
    return "Fourchette {n°"+numero+"}";
  }

}
