package fr.upssitech.tp2.ex1;

import java.util.List;
import java.util.stream.IntStream;

public class Manager {

  private static final int NB_FOURCHETTE = 5;

  public static void main(String[] args) {
    // méthode rapide avec l'API Stream Java 8
    List<Fourchette> fourchettes = IntStream
                  .range(0, NB_FOURCHETTE)
                  .mapToObj(Fourchette::new)
                  .toList();
    List<Philosophe> philosophes = IntStream
                  .range(0, NB_FOURCHETTE)
                  .mapToObj(i -> 
                      new Philosophe(i, fourchettes.get(i), fourchettes.get((i + 1) % NB_FOURCHETTE)))
                  .toList();
    
    for(Philosophe philosophe : philosophes){
      philosophe.start();
    }
  }

  public static void main2(String[] args) {
    // méthode rapide avec l'API Stream Java 8
    Fourchette[] fourchettes = new Fourchette[NB_FOURCHETTE];
    for(int i = 0; i < NB_FOURCHETTE; i++){
      fourchettes[i] = new Fourchette(i);
    }

    Philosophe[] philosophes = new Philosophe[NB_FOURCHETTE];
    for(int i = 0; i < NB_FOURCHETTE; i++){
      if(i == NB_FOURCHETTE - 1){
        // dernier mec
        philosophes[i] = new Philosophe(i, fourchettes[i], fourchettes[0]);
      }else{
        philosophes[i] = new Philosophe(i, fourchettes[i], fourchettes[i+1]);
      }
    }

    for(Philosophe philosophe : philosophes){
      philosophe.start();
    }
  }

}
