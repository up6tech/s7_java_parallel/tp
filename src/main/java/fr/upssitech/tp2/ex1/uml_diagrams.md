### uml: class diagram
```plantuml
@startuml
package "Exercice 1" #FFF {
    
    class Philosophe << Thread >>{
        + run()
    }

    class Fourchette {
      - prise : boolean
      + estPrise() : boolean
    }

}
@enduml
```