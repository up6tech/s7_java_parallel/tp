package fr.upssitech.tp2.ex1;

public class Philosophe extends Thread {

  private int numero;
  private Fourchette gauche;
  private Fourchette droite;

  public Philosophe (int numero, Fourchette droite, Fourchette gauche){
    this.numero = numero;
    this.droite = droite;
    this.gauche = gauche;
  }

  @Override
  public void run() {
    while(true){
      manger();
      penser();
    }
  }

  private void manger() {
    System.out.println("Le philosophe "+numero+" pense");
    try{
      Thread.sleep(1000);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  private void penser() {
    System.out.println("le philosophe "+numero+" prend sa fourchette droite");
    this.droite.prendre();
    System.out.println("le philosophe "+numero+" prend sa fourchette gauche");
    this.gauche.prendre();
    System.out.println("le philosophe "+numero+" mange");
    try{
      Thread.sleep(1000);
    }catch(Exception e){
      e.printStackTrace();
    }
    System.out.println("le philosophe "+numero+" dépose sa fourchette droite");
    this.droite.déposer();
    System.out.println("le philosophe "+numero+" dépose sa fourchette gauche");
    this.gauche.déposer();
  }

  

  @Override
  public String toString() {
    return "Philosophe {n°"+this.numero+", droite:"+this.droite+", gauche:"+this.gauche+"}";
  }

}
