package fr.upssitech.tp2.ex2;

import java.util.Random;

public class Redacteur extends Thread{
  
  private static final String[] MOTS = new String[]{
    "un", "ratio", "flop", "pleure", "miaule", "petit", "SUUUU",
    "et ce", "???", "Jardel", "Nicolas", "hurle", "hulule",
    "tout dur", "en me réveillant", "bangala", "pd pd",
    "ENCULLL2222", "couine", "beugle", "chouine", "crisse",
    "aboie", "Fabien", "Léni", "Robine", "Baqué", "BELLO BITO",
    "bengalo 9", "sus", "imposteur", "como se dice", "noir"
  };

  private static final Random RANDOM = new Random();

  public static String motAleatoire(){
    return MOTS[RANDOM.nextInt(MOTS.length)];
  }

  public static String phraseAléatoire(int taille){
    if(taille < 1) return "a";
    StringBuilder builder = new StringBuilder();
    for(int i = 0; i < taille; i++)
      builder.append(motAleatoire()+" ");
    return builder.toString();
  }

  private Livre livre;

  private int numero;

  public Redacteur (int numero, Livre livre){
    this.livre = livre;
    this.numero = numero;
  }

  @Override
  public void run() {
    while(true){
      try {
        System.out.println("Le redacteur "+this.numero+ " écrit");
        if(this.livre.isEcriture()){
          System.out.println("Le rédacteur "+this.numero+" attend.");
        }
        this.livre.ecrire(phraseAléatoire(3));
        Thread.sleep(500);
        this.livre.finEcriture();
        System.out.println("Le rédacteur "+this.numero+" se repose SUUUUU");
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
