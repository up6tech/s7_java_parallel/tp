package fr.upssitech.tp2.ex2;

public class Lecteur extends Thread {

  private int numero;
  private Livre livre;

  public Lecteur(int numero, Livre livre) {
    this.livre = livre;
    this.numero = numero;
  }

  @Override
  public void run() {
    while (true) {
      System.out.println("Lecteur n°" + numero + " va lire SUUUUU");
      try {
        this.livre.debutLecture();
        Thread.sleep(500);
        System.out.println("Le contenu : \""+this.livre.getContenu()+"\"");
        this.livre.finLecture();
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
