package fr.upssitech.tp2.ex2;

import java.util.List;
import java.util.stream.IntStream;

public class Main {

  public static void main(String[] args) {

    Livre livre = new Livre();
    List<Redacteur> redacteurs = IntStream
      .range(0, 6)
      .mapToObj(i -> new Redacteur(i, livre)).toList();

    List<Lecteur> lecteurs = IntStream
      .range(0, 6)
      .mapToObj(i -> new Lecteur(i, livre)).toList();

    for(Redacteur r : redacteurs){
      r.start();
    }
    for(Lecteur l : lecteurs){
      l.start();
    }


  }

}
