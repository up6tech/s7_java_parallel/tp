package fr.upssitech.tp2.ex2;

public class Livre {
  
  private String contenu = "";
  private boolean ecriture = false;
  private int nbLecteur = 0;

  public boolean isEcriture() {
    return ecriture;
  }

  public synchronized void ecrire(String contenu){
    while(this.ecriture || this.nbLecteur > 0){
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    this.ecriture = true;
    this.contenu += contenu;
  }

  public synchronized void finEcriture() {
    this.ecriture = false;
    this.notify();
  }

  public String getContenu() {
    return contenu;
  }

  public synchronized void debutLecture(){
    while(this.ecriture){
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    this.nbLecteur++;
  }

  public synchronized void finLecture(){
    this.nbLecteur--;
    if(this.nbLecteur == 0){
      this.notify();
    }
  }

  @Override
  public String toString() {
    return "Livre {'"+this.contenu+"'}";
  }

}
