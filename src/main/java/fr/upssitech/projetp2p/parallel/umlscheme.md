### uml: class diagram

```plantuml
@startuml
package "Etape 2" #EEE {

    class ServeurFTP{
      + main()
      + handleClient(Socket soc, FTPProtocole prot, FTPConnection con)
    }

    class ClientFTP {
      + main()
    }

    class FTPManager {
      + List<File> list(FTPConnection con)
      + boolean contains(FTPConnection con, String dir)
      + File getFile(FTPConnection con, String file)
      + boolean isDirectory(FTPConnection con, String file)
    }

    class FTPConnection{
      - dossierActuel : String
      + getDossierActuel() : String
      + setDossierActuel(String dossier)
    }

    class FTPProtocole{
      + String commande(con connection, String command, Socket soc)
    }

    FTPProtocole --> FTPManager


}
@enduml
```
