package fr.upssitech.projetp2p.parallel;

import java.io.File;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class FTPProtocole {

  public static final int FTP_PROTOCOL_PORT = 56987;
  public static final int MAX_BLOCK_SIZE_IN_BYTES = 4 * 1024;
  private List<Integer> usedPorts = new Vector<>();

  private FTPManager ftpManager;

  public FTPProtocole(FTPManager ftpManager) {
    this.ftpManager = ftpManager;
  }

  public String commande(String commande, final Socket tcpSocket) {
    // Args représente les élèments de la commande sous un tableau de chaîne de
    // caractère
    // => Si la commande c'est "download pom.xml 1"
    // Alors le contenu du tableau sera '["download", "pom.xml", "1"]'
    String[] args = commande.split("\\s+");
    switch (args[0]) {
      case "?":
      case "help":
      case "HELP":
        // Renvoie la liste des commandes
        return FTPManager.HELP_STR;
      case "BYTECOUNT":
      case "bc":
      case "ratio":
        try {
          // retourne le nombre de byte d'un fichier
          // Aide : voir la partie download
          String file = args[1];
          if (!ftpManager.contains(file)) {
            return "ERREUR fichier '" + file + "' inexistant";
          }
          if (ftpManager.isDirectory(file)) {
            return "ERREUR fichier '" + file + "' inexistant";
          }
          long nbByte = ftpManager.getFile(file).length();
          return "OK " + nbByte;
        } catch (ArrayIndexOutOfBoundsException e) {
          return "ERREUR le nom du fichier est manquant";
        }
      case "dwn":
      case "cp":
      case "get":
      case "download":
      case "DWN":
        // Permet le téléchargement d'un fichier
        try {
          String fileAsk = args[1];
          // Si l'utilisateur rentre un fichier qui n'est pas présent
          // dans son dossier actuel (relative à la connexion tcp)
          // alors on lui dit fichier inexistant
          if (!ftpManager.contains(fileAsk)) {
            return "ERREUR fichier '" + fileAsk + "' inexistant";
          }
          if (ftpManager.isDirectory(fileAsk)) {
            return "ERREUR la cible est un dossier";
          }
          int numeroBlock = Integer.parseInt(args[2]);
          int rPort = Utils.nbrRand(1_500, 60_000);
          while (usedPorts.contains(rPort) || !Utils.available(rPort)) {
            rPort = Utils.nbrRand(1_500, 60_000);
          }
          usedPorts.add(rPort);

          final int randomPort = rPort;
          new Thread(() -> {
            System.out.println("-> Opening data server on " + randomPort);
            try (var fileSocket = new ServerSocket(randomPort)) {
              Socket transfert = fileSocket.accept();
              // Construction d'un PrintStream pour envoyer du texte à travers la connexion
              //
              PrintStream sortieSocket = new PrintStream(transfert.getOutputStream());
              // Open file

              // byte[] content = Files.readAllBytes(ftpManager.getFile(ftpConnection,
              // file).toPath());

              byte[] content = this.ftpManager.readByteFromFile(
                  MAX_BLOCK_SIZE_IN_BYTES, numeroBlock, ftpManager.getFile(fileAsk));

              sortieSocket.println(content.length);
              // Transfert le fichier d'un coup
              Thread.sleep(100);
              transfert.getOutputStream().write(content);
              transfert.getOutputStream().flush();
              // close transfert
              if(!transfert.isClosed()){
                transfert.close();
              }
              usedPorts.remove((Integer) randomPort);
              System.out.println("<- Closing data server on " + randomPort);
              if(!tcpSocket.isClosed()){
                PrintStream sortieSocketPrincipale = new PrintStream(tcpSocket.getOutputStream());
                sortieSocketPrincipale.println("STOP");
              }
              
            } catch (Exception e) {
              e.printStackTrace();
            }
          }).start();
          return "OK OPENDOWNLOAD " + rPort;
        } catch (ArrayIndexOutOfBoundsException e) {
          return "ERREUR le nom du fichier est manquant";
        }
      case "ls":
      case "list":
      case "saliou":
      case "LIST":
        List<File> files = ftpManager.list();
        Collections.sort(files, (file1, file2) -> {
          boolean isDir1 = file1.isDirectory();
          boolean isDir2 = file2.isDirectory();
          if (isDir1 && isDir2) {
            return file1.getName().compareTo(file2.getName());
          }
          int ordre = (isDir1 ? 100 : 0) - (isDir2 ? 100 : 0);
          return file1.getName().compareTo(file2.getName()) - ordre;
        });
        var strBuilder = new StringBuilder();
        strBuilder.append("D\t../\n");
        for (var fileIter : files) {
          boolean isDir = fileIter.isDirectory();
          strBuilder
              .append((isDir ? "D" : "F") + "\t" + fileIter.getName() + "/\n");
        }
        strBuilder.replace(strBuilder.length() - 1, strBuilder.length(), "")
            .append("\n");
        return "OK " + strBuilder.toString();
      default:
        return "ERREUR Commande inconnue";
    }
  }

}
