package fr.upssitech.projetp2p.parallel;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Utils {

    public static <T> String listToString(List<T> list) {
		return String.join(",", list.stream().map(T::toString).toList());
	}

	public static List<Integer> stringToListInts(String list) {
		return Arrays.asList(list.split(",")).stream().map(Integer::valueOf).toList();
	}


    /**
     * Nombre aléatoire
     * 
     * @param min
     * @param max
     * @return
     */
    public static int nbrRand(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }

    /**
     * Element aléatoire d'une liste
     * 
     * @param <T>
     * @param elements
     * @return
     */
    public static <T> T elementAleatoire(List<T> elements) {
        return elements.get(Utils.nbrRand(0, elements.size()));
    }

    /**
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     */
    public static boolean available(int port) {
        if (port < 0 || port > 64500) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }

}
