package fr.upssitech.projetp2p.parallel;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

public class ClientFTP {

  public static final String EOF = String.valueOf('\u001a');

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    var quitFunc = List.of("q", "quit", "exit");

    int defaultPort = FTPProtocole.FTP_PROTOCOL_PORT;
    List<Integer> serverPorts = new ArrayList<>();

    List<String> arguments = Arrays.asList(args);
    // On regarde si l'utilisateur a mis des ports dans la commande
    if (arguments.contains("-p")) {
      String strValue = arguments.get(arguments.indexOf("-p") + 1);
      // On écrit les ports comme ceci :
      // >>>> java -jar ClientFTP.jar -p 1,2,3,4,5
      // On ajoute les ports
      Stream.of(strValue.split(","))
          .mapToInt(Integer::parseInt)
          .forEach(serverPorts::add);

    } else {
      serverPorts.add(defaultPort);
    }

    String chaine = "";
    System.out.println("Tapez vos phrases ou '" + String.join(",", quitFunc) + "' pour arrêter :");
    while (true) {
      // lecture clavier
      System.out.print("<- : ");

      chaine = scanner.nextLine();
      if (quitFunc.contains(chaine.toLowerCase())) {
        System.out.println("FIN programme");
        break;
      }

      int randomServer = Utils.nbrRand(0, serverPorts.size());
      int portServer = serverPorts.get(randomServer);
      System.out.println("Choix du serveur n°" + randomServer + " (Port : " + portServer + ")");
      String[] argList = chaine.split("\\s+");
      String commande = argList[0];

      switch (commande) {
        case "dwn":
        case "cp":
        case "get":
        case "download":
        case "DWN":
          // case download
          final String file = argList[1];
          // Lorsque le client veut télécharger :
          // Alors
          int byteSize = -1;
          try (Socket askFileSizeSocket = new Socket("localhost", portServer)) {
            // On demande le nombre de byte du fichier
            BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(askFileSizeSocket.getInputStream()));

            PrintStream sortieSocket = new PrintStream(askFileSizeSocket.getOutputStream());
            sortieSocket.println("BYTECOUNT " + file + "\n");
            // On récup la sortie

            String byteCountAsString = entreeSocket.readLine();
            // Reçu = return "OK " + nbByte
            // on extrait le nombre de byte
            byteSize = Integer
                .parseInt(
                    byteCountAsString.substring(3)
                        .replace(EOF, "")
                        .trim()); // test run dans le le package de test, classe GestionProtocoleTest
            System.out.println("SERVER -> " + byteCountAsString);
            System.out.println("File length is " + byteSize + " bytes");

            sortieSocket.println("STOP");

            askFileSizeSocket.close();
          } catch (Exception e) {
            e.printStackTrace();
          }
          if (byteSize == -1) {
            System.out.println("Erreur de lecture du nombre de byte d'un fichier, voir console (ratio)");
            continue;
          }
          final int BLOCK_SIZE = FTPProtocole.MAX_BLOCK_SIZE_IN_BYTES;
          // On créer un fichier vide de taille byteSize
          File fichier = new File(file);
          /*
           * int bufferSize = 1024;
           * try (FileOutputStream outStream = new FileOutputStream(fichier)) {
           * byte[] buffer = new byte[bufferSize];
           * int totalWrite = 0;
           * while (totalWrite < byteSize) {
           * int remainingByte = byteSize - totalWrite;
           * int maxWriteByte = Math.min(bufferSize, remainingByte);
           * if (maxWriteByte == bufferSize) {
           * outStream.write(buffer);
           * } else {
           * outStream.write(new byte[maxWriteByte]);
           * }
           * totalWrite += maxWriteByte;
           * }
           * } catch (Exception e) {
           * e.printStackTrace();
           * }
           */

          // On calcule le nombre de bloc nécessaire et on attribut les blocks
          int totalAmountOfBlocks = (int) Math.ceil(Double.valueOf(byteSize) / Double.valueOf(BLOCK_SIZE));
          // -1 car on commence à 0
          // On lie un numéro de bloc, a un numéro de port serveur (de manière aléatoire)
          // Ensuite on regroupe les numéros de blocs / serveurs
          // Map<ServeurPort, List<NumeroDeBloc>> blocs
          // blocs.get(Serveur numéro 0) => [0, 4, 6]
          // blocs.get(Serveur numéro 1) => [1, 3, 7]

          // Lit un port d'un serveur à une liste de bloc à télécharger
          Map<Integer, List<Integer>> portServeurVersBlocs = new HashMap<>();

          for (int numeroBloc = 0; numeroBloc < totalAmountOfBlocks; numeroBloc++) {
            int numeroServeur = Utils.nbrRand(0, serverPorts.size());
            int portServeur = serverPorts.get(numeroServeur);
            if (portServeurVersBlocs.containsKey(portServeur)) {
              portServeurVersBlocs.get(portServeur).add(numeroBloc);
            } else {
              List<Integer> listeDesBlocs = new ArrayList<>();
              listeDesBlocs.add(numeroBloc);
              portServeurVersBlocs.put(portServeur, listeDesBlocs);
            }
          }

          System.out.println(
              "-> Blocks are " + BLOCK_SIZE + " bytes length each. Downloading " + totalAmountOfBlocks
                  + " blocks.");
          for (int portServeur : portServeurVersBlocs.keySet()) {
            StringBuilder strBuilder = new StringBuilder("\t->Server on port " + portServeur + " will download blocs ");
            List<Integer> listeDeBlocsATelecharger = portServeurVersBlocs.get(portServeur);
            if (listeDeBlocsATelecharger.size() == 1) {
              strBuilder.append(listeDeBlocsATelecharger.get(0));
            } else {
              for (int i = 0; i < listeDeBlocsATelecharger.size(); i++) {

                if (i == listeDeBlocsATelecharger.size() - 1) {
                  strBuilder.append("and " + listeDeBlocsATelecharger.get(i));
                } else {
                  strBuilder.append(listeDeBlocsATelecharger.get(i) + " ");
                }
              }
            }

            System.out.println(strBuilder.toString());
          }

          for (int portServeur : portServeurVersBlocs.keySet()) {
            List<Integer> listeDeBlocsATelecharger = portServeurVersBlocs.get(portServeur);
            // On se connecte
            // On demande les blocs
            // On se déconnecte

            new Thread(() -> {
              try (Socket downloadSocket = new Socket("localhost", portServeur)) {
                // Construction d'un BufferedReader pour lire du texte envoyé à travers la
                // connexion socket
                BufferedReader in = new BufferedReader(new InputStreamReader(downloadSocket.getInputStream()));
                // Construction d'un PrintStream pour envoyer du texte à travers la connexion
                // socket
                PrintStream out = new PrintStream(downloadSocket.getOutputStream());

                for (int numeroBloc : listeDeBlocsATelecharger) {
                  // System.out.println("... Downloading " + numeroBloc + " from server " +
                  // portServeur);
                  out.println("download " + file + " " + numeroBloc);

                  String response = in.readLine();
                  // Buffer filled with random stuff OMEGALUL
                  while (response.equals("STOP")) {
                    response = in.readLine(); // on vide le buffer petit flop
                  }
                  int connectPort = Integer.parseInt(response.substring("OK OPENDOWNLOAD ".length())
                      .replace(EOF, "")
                      .trim());

                  // System.out.println("... Trying to connect to " + connectPort);
                  Thread.sleep(25);
                  try (Socket dataSocket = new Socket("localhost", connectPort)) {
                    BufferedReader inData = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
                    int bufferSizeData = Integer.parseInt(inData.readLine());
                    // System.out.println("Transfering "+bufferSizeData+" bytes to file");
                    int offset = BLOCK_SIZE * numeroBloc;
                    byte[] blocBytes = dataSocket.getInputStream().readNBytes(bufferSizeData);

                    insert(fichier, offset, blocBytes);
                    dataSocket.close();
                    // System.out.println(" -> Received block " + numeroBloc + " from server " +
                    // portServeur);
                  } catch (Exception e) {
                    e.printStackTrace();
                  }

                  /*
                   * String closeWait = in.readLine();
                   * if (closeWait.equals("STOP"))
                   * continue;
                   */
                }
                downloadSocket.close();
              } catch (Exception e) {
                e.printStackTrace();
              }
            }).start();
          }
          break;

        // Si la commande c'est ls ou help ou jsais pas quoi
        default:
          try (Socket tcpClient = new Socket("localhost", portServer)) {
            // Construction d'un BufferedReader pour lire du texte envoyé à travers la
            // connexion socket
            BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpClient.getInputStream()));
            // Construction d'un PrintStream pour envoyer du texte à travers la connexion
            // socket
            PrintStream sortieSocket = new PrintStream(tcpClient.getOutputStream());

            sortieSocket.println(chaine);

            // lecture d'une chaine envoyée à travers la connexion socket
            while (true) {
              chaine = entreeSocket.readLine();
              if (chaine == null || chaine.equals("STOP")) {
                break;
              }

              if (chaine.endsWith(EOF)) {
                System.out.println("-> : " + chaine);
                break;
              } else {
                System.out.println(chaine);
              }
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
          break;
      }
    }
    scanner.close();
  }

  private static void insert(File file, long offset, byte[] content) throws IOException {

    RandomAccessFile r = new RandomAccessFile(file, "rw");
    r.seek(offset);
    r.write(content);
    r.close();
  }
}
