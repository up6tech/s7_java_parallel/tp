package fr.upssitech.projetp2p.parallel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

// a faire :
// FTPProtocole ligne 58
//(serveur permet de connaitre) taille du fichier 
//(serveur passe de lire un fichier a lire des blocs)

public class ServeurFTP {

  public static void main(String[] args) {

    int defaultPort = FTPProtocole.FTP_PROTOCOL_PORT;
    // On vérifie si l'utilisateur a lancer le programme avec un paramètre pour le
    // port
    List<String> arguments = Arrays.asList(args);
    if (arguments.contains("-p")) {
      String strValue = arguments.get(arguments.indexOf("-p") + 1);
      defaultPort = Integer.valueOf(strValue);
    }

    FTPManager ftpManager = new FTPManager();
    FTPProtocole ftpProtocole = new FTPProtocole(ftpManager);

    try (var tcpSocket = new ServerSocket(defaultPort)) {
      System.out.println("Starting FTP server on port " + defaultPort);
      System.out.println("using TCP");
      while (true) {
        var clientSocket = tcpSocket.accept();
        System.out.println("Client connected ...");
        new Thread(() -> {
          try {
            handleClient(clientSocket, ftpProtocole);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }).start();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void handleClient(Socket tcpSocket, FTPProtocole ftpProtocole) throws IOException {
    // Construction d'un BufferedReader pour lire du texte envoyé à travers la
    // connexion socket
    BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
    // Construction d'un PrintStream pour envoyer du texte à travers la connexion
    // socket
    PrintStream sortieSocket = new PrintStream(tcpSocket.getOutputStream());

    String input = "";
    while (input != null) {
      if (tcpSocket.isClosed()) {
        break;
      }
      input = entreeSocket.readLine();
      if (input != null) {
        if (input.equals("STOP")) {
          System.out.println(tcpSocket.getInetAddress() + " closed");
          break;
        }

        System.out.println(tcpSocket.getInetAddress() + " -> SERVER : " + input);
        String result = ftpProtocole.commande(input, tcpSocket) + '\u001a';
        System.out.println("SERVER -> " + tcpSocket.getInetAddress() + " : " + result);
        sortieSocket.println(result);
      } else
        break; // input = null on sort de la boucle car la connexion est rompue
    }

    tcpSocket.close();
  }

}
