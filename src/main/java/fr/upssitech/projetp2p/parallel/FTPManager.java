package fr.upssitech.projetp2p.parallel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FTPManager {

  
  public static final String HELP_STR =
    """
      Voici la liste des commandes disponible sur ce serveur:
      LIST,list,ls    - Voir la list des fichiers ET dossiers 
      DWN,get,cp,download  - Télécharge un fichier vers votre machine
      BYTECOUNT,bc    - Retourne le nombre de byte d'un fichier

    """;

  public static final String DOWNLOAD_DIR = "./";
    

  /**
   * Renvoie la liste des fichiers contenu dans le répertoire de l'utilisateur
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @return La liste des fichiers
   */
  public List<File> list() {
    File file = new File(DOWNLOAD_DIR);
    return Arrays.asList(file.listFiles());
  }

  /**
   * Vérifie si un dossier est contenu dans le répertoire de l'utilisateur courrant
   * @param ftpConnection Donnée de l'utilisateur (connexion tcp)
   * @param dir dossier à vérifier
   * @return True si le dossier est contenu, False sinon
   */
  public boolean contains(String dir) {
    if(dir.equals("..")) return true;
    File userDir = new File(DOWNLOAD_DIR);
    // On obtient la liste des noms de fichier / dossiers 
    // dans le dossier utilisateur
    String[] fileArray = userDir.list();
    // On passe du tableau à l'objet
    /*
    Le code en dessous est équivalent à ce truc (en C)
    for (int i = 0; i < fileArray.length; i++) {
      if(fileArray[i].equals(dir)){
        return true;
      }
    }
    return false;
    */

    List<String> fileList = Arrays.asList(fileArray);
    return fileList.contains(dir);
  }

  /**
   * Transforme une chaîne de caractère en objet File de java
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @param file le nom du fichier
   * @return L'objet fichier
   */
  public File getFile(String file){
    return new File(DOWNLOAD_DIR+File.separator+file);
  }

  /**
   * Lecture du fichier bloc par bloc 
   * @param blockSize taille du bloc
   * @param blockNumber numero du bloc
   * @param file lien vers le fichier
   * @return Le tableau de byte lu
   * @throws IOException
   */
  public byte[] readByteFromFile(int blockSize, int blockNumber, File file) 
        throws IOException {
    // On ouvre le fichier sous forme de flux de byte
    FileInputStream input = new FileInputStream(file);
    // On demande la taille max du fichier
    long totalByteLength = input.getChannel().size();
    // On calcul le décalage que notre curseur va devoir faire
    int offset = blockSize * blockNumber;
    if(offset > totalByteLength){
      // On essaye de chercher un bloc qui est en dehors du fichier
      // ex : bloc n°2 de taille de 4Ko et le fichier fait 1 octet
      input.close();
      return new byte[]{};
    }
    // On déplace le curseur de lecture au début du bloc demandé
    input.skipNBytes(offset);
    if(offset + blockSize > totalByteLength){
      // On essaye de chercher le dernier block, mais la taille du fichier
      // n'est pas un multiple de la taille du block
      // ex : la taille du fichier est de 9Ko, et on découpe par tranche de 2Ko
      blockSize = (int) (totalByteLength - offset);
    }
    byte[] bytes = new byte[blockSize];
    input.read(bytes);
    input.close();
    return bytes;
  }

  /**
   * Vérifie si le nom entrée en paramètre est un dossier
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @param file le nom du fichier
   * @return L'objet fichier
   */
  public boolean isDirectory(String file) {
    return getFile(file).isDirectory();
  }
}
