package fr.upssitech.projetp2p.p2pcoop.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichier;

public class P2PLocalFileManager {

  private String folder;
  private int tailleBloc;

  public P2PLocalFileManager(int tailleBloc, String folder) {
    this.tailleBloc = tailleBloc;
    this.folder = folder;
  }

  private String fichier(String fichier) {
    return this.folder + File.separatorChar + fichier;
  }

  /**
   * 
   * @throws IOException
   */
  public void initialiserLesFichiers() throws IOException {
    // Regarder tous les fichiers contenu dans le dossier
    // Si leur fichier de metadata n'existe pas
    // On créer le fichier de metadata sur ce fichier, avec 100% des blocs
    // disponibles
    List<String> listfichier = listeFichier();

    for (String fichier : listfichier) {
      if (fichier.endsWith(".metaprops"))
        continue;
      if (!possedeMetadata(fichier)) {
        System.out.println("[FileManager] Création metaprops "+fichier);
        File metaFile = new File(fichier(fichier) + ".metaprops");
        metaFile.createNewFile();
        long tailleFichier = new File(fichier(fichier)).length();
        setTailleFichier(fichier, tailleFichier);
        // On écrit les blocs
        List<Integer> tousLesBlocs = new ArrayList<>();
        // On calcule le nombre de bloc qu'il faut
        int maxBlocsCount = (int) Math.floor(tailleFichier / Double.valueOf(this.tailleBloc));
        for (int numBloc = 0; numBloc <= maxBlocsCount; numBloc++)
          tousLesBlocs.add(numBloc);
        // Il faut ajouter les blocs dans le fichier de config
        String blockString = String.join(",", tousLesBlocs.stream().map(i -> i.toString()).toList());
        setInfoFichier(fichier, "blocks", blockString);
      }
    }
  }

  /**
   * 
   * @param fichier
   * @param donnee  Si true, alors les données seront inclus, sinon il n'y aura
   *                que les numéros des blocs
   * @return
   * @throws NumberFormatException
   * @throws IOException
   */
  public InfoFichier infoFichier(String fichier, boolean donnee) throws NumberFormatException, IOException {
    if (!existe(fichier)) {
      return InfoFichier.VIDE;
    }
    long tailleFichier = getTailleFichier(fichier);
    Map<Integer, byte[]> blocks = new HashMap<>();

    int maxBlocsCount = (int) Math.floor(tailleFichier / Double.valueOf(this.tailleBloc));
    for (int numBloc = 0; numBloc <= maxBlocsCount; numBloc++) {
      if (donnee) {
        blocks.put(numBloc, blocFichier(fichier, numBloc));
      } else {
        blocks.put(numBloc, new byte[] {});
      }
    }

    return new InfoFichier(fichier, tailleFichier, blocks);
  }

  /**
   * Vérifie si un fichier est contenu dans le dossier de lancement
   * 
   * @param nomFichier Le nom du fichier
   * @return
   */
  public boolean existe(String nomFichier) {
    File userDir = new File(folder);
    // On obtient la liste des noms des fichiers
    // dans le dossier utilisateur
    String[] fileArray = userDir.list();

    List<String> fileList = Arrays.asList(fileArray);
    return fileList.contains(nomFichier);
  }

  public List<String> listeFichier() {
    return Arrays.asList(new File(folder).list());
  }

  /**
   * Récupérer un certains bloc d'un fichier
   * 
   * @param fichier
   * @param numeroBloc
   * @return
   * @throws IOException
   */
  public byte[] blocFichier(String file, int numeroBloc) throws IOException {
    // retourne le bloc d'un fichier
    // On ouvre le fichier sous forme de flux de byte
    FileInputStream input = new FileInputStream(fichier(file));
    // On demande la taille max du fichier
    long totalByteLength = input.getChannel().size();
    int localTailleBloc = tailleBloc;
    // On calcul le décalage que notre curseur va devoir faire
    int offset = localTailleBloc * numeroBloc;
    if (offset > totalByteLength) {
      // On essaye de chercher un bloc qui est en dehors du fichier
      // ex : bloc n°2 de taille de 4Ko et le fichier fait 1 octet
      input.close();
      return new byte[] {};
    }
    // On déplace le curseur de lecture au début du bloc demandé
    input.skipNBytes(offset);
    if (offset + localTailleBloc > totalByteLength) {
      // On essaye de chercher le dernier block, mais la taille du fichier
      // n'est pas un multiple de la taille du block
      // ex : la taille du fichier est de 9Ko, et on découpe par tranche de 2Ko
      localTailleBloc = (int) (totalByteLength - offset);
    }
    byte[] bytes = new byte[localTailleBloc];
    input.read(bytes);
    input.close();
    return bytes;
  }

  /**
   * Méthode pour insérer un bloc dans un fichier
   * 
   * @param file
   * @param numeroBloc
   * @param donnee
   * @throws IOException
   */
  public void insererBlocDansFichier(String file, int numeroBloc, byte[] donnee) throws IOException {
    RandomAccessFile r = new RandomAccessFile(fichier(file), "rw");
    r.seek(numeroBloc * tailleBloc);
    r.write(donnee);
    r.close();

    List<Integer> blocs = listeBlocPossedes(file);
    if (!blocs.contains(numeroBloc)) {
      // On a jamais eu ce bloc, alors on l'ajoute SUUUUUU
      blocs.add(numeroBloc);
    }
    // On transforme notre liste de blocs en chaîne de caractère
    // [1, 2, 3, 5] => "1,2,3,5"
    String blockString = String.join(",", blocs.stream().map(i -> i.toString()).toList());
    setInfoFichier(file, "blocks", blockString);
  }

  /**
   * 
   * @param file Le fichier
   * @return La liste des blocs sur notre machine
   * @throws IOException
   */
  public List<Integer> listeBlocPossedes(String file) throws IOException {
    String fichier = fichier(file);
    System.out.println("Local: liste bloc possède fichier : " + fichier);
    try {
      List<Integer> blocks = new ArrayList<>();
      String rawBlocks = getInfoFichier(file, "blocks");
      System.out.println("Local: liste des blocs dans le fichier de props: '" + rawBlocks + "'");
      if (rawBlocks == null) {
        return blocks;
      }
      String[] blocksStrings = rawBlocks.split("\\,");
      for (String blocksString : blocksStrings) {
        blocks.add(Integer.valueOf(blocksString));
      }
      return blocks;
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      return new ArrayList<>(); // Liste vide
    }
  }

  /**
   * Récupère la taille du fichier total (Par exemple si on a pas le fichier au
   * complet on aimerai
   * savoir sa taille)
   * 
   * @param fileName
   * @return La taille du fichier en octets
   * @throws NumberFormatException
   * @throws IOException
   */
  public long getTailleFichier(String fileName) throws NumberFormatException, IOException {
    return Long.valueOf(getInfoFichier(fileName, "size"));
  }

  /**
   * Mettre la taille du fichier à jour
   * 
   * @param fileName
   * @param tailleEnOctets
   * @throws IOException
   */
  public void setTailleFichier(String fileName, long tailleEnOctets) throws IOException {
    setInfoFichier(fileName, "size", String.valueOf(tailleEnOctets));
  }

  /**
   * 
   * @param file
   * @param cle
   * @return Null si le fichier n'existe pas Ou si la clé n'existe pas, sinon
   *         renvoie la valeur
   * @throws IOException
   */
  public String getInfoFichier(String file, String cle) throws IOException {
    File fileProperties = new File(fichier(file) + ".metaprops");
    if (!fileProperties.exists()) {
      fileProperties.createNewFile();
      return null;
    }
    Properties fileProps = new Properties();
    fileProps.load(new FileInputStream(fileProperties));
    return fileProps.getProperty(cle);
  }

  /**
   * 
   * @param file
   * @return
   */
  public boolean possedeMetadata(String file) {
    File metaFile = new File(file + ".metaprops");
    return metaFile.exists();
  }

  /**
   * 
   * @param file
   * @param cle
   * @param valeur
   * @throws IOException Si file est null
   */
  public void setInfoFichier(String file, String cle, String valeur) throws IOException {
    File fileProperties = new File(fichier(file) + ".metaprops");
    if (!fileProperties.exists()) {
      fileProperties.createNewFile();
    }
    Properties fileProps = new Properties();
    fileProps.load(new FileInputStream(fileProperties));
    fileProps.setProperty(cle, valeur);
    fileProps.store(new FileOutputStream(fileProperties), null);
  }

  /**
   * 
   * @param nomFichier le nom du fichier
   * @return True si on possède tous les blocs du fichier
   * @throws NumberFormatException
   * @throws IOException           S'il y a eu une erreur sur la lecture de
   *                               fichiers
   */
  public boolean possedeTousLesBlocs(String nomFichier) throws NumberFormatException, IOException {
    long taille = getTailleFichier(nomFichier);
    int maxBlocsCount = (int) Math.floor(taille / Double.valueOf(this.tailleBloc));
    return listeBlocPossedes(nomFichier).size() == maxBlocsCount + 1;
  }

}
