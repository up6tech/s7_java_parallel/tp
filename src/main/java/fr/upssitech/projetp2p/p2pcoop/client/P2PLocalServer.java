package fr.upssitech.projetp2p.p2pcoop.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichier;

public class P2PLocalServer extends Thread {

  private final int listenPort;
  private final P2PLocalFileManager fileManager;

  public P2PLocalServer(int listenPort, P2PLocalFileManager fileManager) {
    this.listenPort = listenPort;
    this.fileManager = fileManager;
  }

  @Override
  public void run() {
    System.out.println("[ServeurLocal] Initialisation du serveur ...");
    System.out.println("[ServeurLocal] Ecoute sur le port : " + this.listenPort);
    try (ServerSocket serverSocket = new ServerSocket(this.listenPort)) {

      while(true){
        Socket socket = serverSocket.accept();
        new AccepterClient(socket).start();
      }

    } catch (Exception e) {
      e.printStackTrace(System.err);
      System.err.println("[ServeurLocal] Erreur sur l'écoute du serveur");
    }
  }

  private class AccepterClient extends Thread {

    private Socket client;

    AccepterClient(Socket client) {
      this.client = client;
    }

    @Override
    public void run() {
      try {
        BufferedReader lecture = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintStream ecriture = new PrintStream(client.getOutputStream());

        String first = lecture.readLine();

        if (first.equals("registrar")) {
          System.out.println("[ServeurLocal] Demande du registrar");
          // Ici, c'est le registrar qui demande un truc
          // on le traite (on parle pas du n)
          String inputCommand = lecture.readLine();
          if (inputCommand.startsWith("exists file ")) {
            // Ask for file existence
            String askedFile = inputCommand.substring("exists file ".length());
            System.out.println("[ServeurLocal] Demande existence du fichier '"+askedFile+"'");
            if (!fileManager.existe(askedFile)) {
              ecriture.println("no");
              System.out.println("[ServeurLocal] Le fichier n'existe pas");
              this.client.close();
              return;
            }
            InfoFichier info = fileManager.infoFichier(askedFile, false);
            if (info == InfoFichier.VIDE) {
              System.out.println("[ServeurLocal] Le fichier n'existe pas");
              ecriture.println("no");
              this.client.close();
              return;
            } else {
              System.out.println("[ServeurLocal] Transmission du fichier "+info);
              ecriture.println(info.serial());
            }
          }
        }else if(first.equals("download")){
          System.out.println("[ServeurLocal] Demande d'un client...");
          String fileName = lecture.readLine();
          int askedBlockCount = Integer.parseInt(lecture.readLine());
          System.out.println("[ServeurLocal] Demande de "+askedBlockCount+" blocs du fichier "+fileName);
           for(int i = 0; i < askedBlockCount; i++){
            System.out.println("[ServeurLocal] Attente d'un bloc ...");
             int blocNumber = Integer.parseInt(lecture.readLine());
             System.out.println("[ServeurLocal] Demande par le client du bloc "+blocNumber);

             byte[] rawBlockData = P2PLocalServer.this.fileManager.blocFichier(fileName, blocNumber);
             System.out.println("[ServeurLocal] Envoie d'un bloc de taille "+rawBlockData.length);
             ecriture.println(Integer.toString(rawBlockData.length));
             Thread.sleep(100);
             client.getOutputStream().write(rawBlockData);
             System.out.println("[ServeurLocal] Envoie effectué");
           }

        }
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    }

  }

}
