package fr.upssitech.projetp2p.p2pcoop.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.upssitech.projetp2p.p2pcoop.client.P2PApp;
import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichier;
import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichierTransfert;

public class P2PKeeper {

  private List<P2PRemoteClient> connectedClients = new ArrayList<>();

  public boolean isAvalaible(int port) {
    for (P2PRemoteClient client : connectedClients) {
      if (client.getPort() == port)
        return false;
    }
    return true;
    /*
     * return !connectedClients
     * .stream()
     * .anyMatch(p2pClient -> p2pClient.getPort() == port);
     */
  }

  public InfoFichierTransfert infoTransfert(String fileName, int portAsking) {
    long taille = 0;
    // La structure de donnée "Set" n'autorise pas de doublons
    Map<Integer, List<Integer>> blocsByClient = new HashMap<>();
    for (P2PRemoteClient remoteClient : connectedClients) {
      if (remoteClient.getPort() == portAsking) {
        // On ne va pas voir le client qui demande un fichier
        continue;
      }
      // Pour chaque client connecté au registrar
      try {
        System.out.println("[Registrar] Demande info fichier client sur le port " + remoteClient.getPort());
        InfoFichier infoFichier = remoteClient.possedeLeFichier(fileName);
        if (infoFichier != InfoFichier.VIDE) {
          if (taille == 0) { // Si on a pas encore trouvé un client qui a des infos
            taille = infoFichier.getFileSize();
          }
          // Ajout la liste des blocs possèdé par ce client
          // dans la liste des blocs à transmettre
          System.out.println("[Registrar] Info : " + infoFichier);
          blocsByClient.put(remoteClient.getPort(), infoFichier.getListeBlocs());
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (taille == 0 || blocsByClient.isEmpty()) {
      // Ici, aucun client n'a le fichier
      System.out.println("[Registrar] Aucune infos sur le fichiers '" + fileName + "' (pour "
          + this.connectedClients.size() + " clients)");
      return InfoFichierTransfert.VIDE;
    }
    // On réorganiser pour répartir équitablement les blocs entre clients

    List<Integer> allBlocksAvalaibles = blocsByClient.values().stream().flatMap(Collection::stream)
        .distinct()
        .toList();

    // Distribution de manière équitable des blocs
    // Selon un round robin
    Map<Integer, List<Integer>> distributedBlocsByClient = new HashMap<>();
    blocsByClient.keySet().forEach(k -> distributedBlocsByClient.put(k, new ArrayList<>()));

    List<Integer> clients = new ArrayList<>(blocsByClient.keySet());
    int index = 0;
    for (int bloc : allBlocksAvalaibles) {
      int portOfClient = clients.get(index);
      // S'il n'est pas contenu, on ne peut pas lui attribuer
      // Donc on peut attribuer un bloc a un client que s'il le possède
      if (blocsByClient.get(portOfClient).contains(bloc)) {
        distributedBlocsByClient.get(portOfClient).add(bloc);
      }
      index++;
      if (index >= clients.size())
        index = 0;

    }

    InfoFichierTransfert infoFichier = new InfoFichierTransfert(fileName, taille, distributedBlocsByClient);
    System.out.println("[Registrar] Le fichier a été récupéré : " + infoFichier);
    return infoFichier;
  }

  /**
   * 
   * @param fileName
   * @return InfoFichier
   */
  public InfoFichier infoFichier(String fileName) {
    long taille = 0;
    // La structure de donnée "Set" n'autorise pas de doublons
    Set<Integer> blocs = new HashSet<>();
    // https://gitlab.com/up6tech/s7_java_parallel/tp/-/blob/main/src/main/java/fr/upssitech/projetp2p/p2pcoop/umldiagrams.md
    for (P2PRemoteClient remoteClient : connectedClients) {
      // Pour chaque client connecté au registrar
      try {
        System.out.println("[Registrar] Demande info fichier client sur le port " + remoteClient.getPort());
        InfoFichier infoFichier = remoteClient.possedeLeFichier(fileName);
        if (infoFichier != InfoFichier.VIDE) {
          if (taille == 0) { // Si on a pas encore trouvé un client qui a des infos
            taille = infoFichier.getFileSize();
          }
          // Ajout la liste des blocs possèdé par ce client
          // dans la liste des blocs à transmettre
          System.out.println("[Registrar] Info : " + infoFichier);
          blocs.addAll(infoFichier.getListeBlocs());
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (taille == 0 || blocs.isEmpty()) {
      // Ici, aucun client n'a le fichier
      System.out.println("[Registrar] Aucune infos sur le fichiers '" + fileName + "' (pour "
          + this.connectedClients.size() + " clients)");
      return InfoFichier.VIDE;
    }
    InfoFichier infoFichier = new InfoFichier(fileName, taille, new ArrayList<>(blocs));
    System.out.println("[Registrar] Le fichier a été récupéré : " + infoFichier);
    return infoFichier;
  }

  public void addClient(P2PRemoteClient client) {
    System.out.println("[Registrar] Client enregistré sur le port " + client.getPort());
    this.connectedClients.add(client);
  }

  public void removeClient(P2PRemoteClient client) {
    System.out.println("[Registrar] Client déconnecté sous le port " + client.getPort());
    this.connectedClients.remove(client);
  }

}