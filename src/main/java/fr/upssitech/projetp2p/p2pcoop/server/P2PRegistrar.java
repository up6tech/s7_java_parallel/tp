package fr.upssitech.projetp2p.p2pcoop.server;

import java.net.ServerSocket;
import java.net.Socket;

public class P2PRegistrar implements Runnable {

  public static final int REGISTRAR_PORT = 54789;

  public static void main(String[] args) {
    new P2PRegistrar()
        .run();
  }

  private P2PKeeper keeper;

  public P2PRegistrar() {
    this.keeper = new P2PKeeper();
  }

  /**
   * @return the keeper
   */
  public P2PKeeper getKeeper() {
    return keeper;
  }

  @Override
  public void run() {
    System.out.println("[Registrar] Démarrage du registrar sur le port " + REGISTRAR_PORT);
    try (ServerSocket registrarSocket = new ServerSocket(REGISTRAR_PORT)) {

      while (true) {
        // un client se connect est demande l'enregistrement
        Socket socket = registrarSocket.accept();
        System.out.println("[Registrar] Reception client ");
        P2PRemoteClient client = new P2PRemoteClient(this, socket, 0);
        client.start();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
