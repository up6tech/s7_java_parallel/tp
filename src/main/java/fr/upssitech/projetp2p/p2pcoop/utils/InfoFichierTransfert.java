package fr.upssitech.projetp2p.p2pcoop.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InfoFichierTransfert {

  public static final InfoFichierTransfert VIDE = new InfoFichierTransfert("none", -1, Map.of());

  public static InfoFichierTransfert deserial(String string) {
    String[] data = string.split("/");
    String fileName = data[0];
    long fileSize = Long.valueOf(data[1]);
    String blocksData = data[2];

    Map<Integer, List<Integer>> blocs = new HashMap<>();
    for (String blockData : blocksData.split("\\@")) {

      String[] subData = blockData.split("\\*");

      int port = Integer.parseInt(subData[0]);
      String[] blocksList = subData[1].split("\\|");

      List<Integer> blocks = Arrays.asList(blocksList).stream().map(Integer::parseInt).toList();

      blocs.put(port, blocks);
    }
    return new InfoFichierTransfert(fileName, fileSize, blocs);
  }

  private String fileName;
  private long fileSize;
  // Dictionnaire liant une addr ip + port à une liste de blocs
  private Map<Integer, List<Integer>> blocsByClient;

  /**
   * 
   * @param fileName
   * @param fileSize
   * @param map
   */
  public InfoFichierTransfert(String fileName, long fileSize, Map<Integer, List<Integer>> map) {
    this.fileName = fileName;
    this.fileSize = fileSize;
    this.blocsByClient = map;
  }

  /**
   * @return the fileName
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * @return the fileSize
   */
  public long getFileSize() {
    return fileSize;
  }

  /**
   * @return the blocsByClient
   */
  public Map<Integer, List<Integer>> getBlocsByClient() {
    return blocsByClient;
  }

  public String serial() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.fileName + "/");
    builder.append(this.fileSize + "/");
    int size = 0;
    for (var entry : this.blocsByClient.entrySet()) {
      // Le client ne possède rien, ou bien le registrar ne veut rien transmettre, donc on skip dans la serialisation
      if(entry.getValue().isEmpty())
        continue;
      builder
          .append(entry.getKey() + "*" + String.join("|", entry.getValue().stream().map(i -> i.toString()).toList()));
      size++;
      if(size < this.blocsByClient.size()){
        builder.append("@");
      }
    }
    return builder.toString();
  }

  @Override
  public String toString() {
    return "{" +
        " fileName='" + getFileName() + "'" +
        ", fileSize='" + getFileSize() + "'" +
        ", blocsByClient='" + getBlocsByClient() + "'" +
        "}";
  }

 

}
