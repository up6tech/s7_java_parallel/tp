package fr.upssitech.projetp2p.p2pcoop.utils;

public record Pair<U, V>(U first, V second) {}