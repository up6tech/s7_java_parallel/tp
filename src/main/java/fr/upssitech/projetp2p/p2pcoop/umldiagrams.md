## Diagramme de cas d'utilisation

```plantuml
@startuml
left to right direction
actor Utilisateur as s

package Registrar{
  usecase "Demander la liste des clients  du <b>'Registrar'</b>" as UC2
  usecase "S'enregistrer au prêt un <b>'Registrar'</b>" as UC1
}

package Client{
  usecase "Demander des fichiers aux clients" as UC3
}

s --> UC2
s --> UC1
s -left-> UC3

(UC2) .> (UC1) : include
(UC3) .> (UC2) : include
@enduml
```

## Diagramme de séquence : enregistrement d'un client au prêt du registrar

```plantuml
@startuml
autonumber
P2PApp -> P2PRegistrar : register
loop while port is used
  activate P2PRegistrar
  P2PRegistrar -> P2PRegistrar : generateRandomPort() : 550
  P2PRegistrar -> P2PKeeper : check for port 550
  activate P2PKeeper
  P2PKeeper --> P2PRegistrar : not used
  deactivate P2PKeeper
end
P2PRegistrar -> P2PKeeper : add client at port 550
P2PRegistrar --> P2PApp : OK 550
deactivate P2PRegistrar
activate P2PApp
P2PApp -> P2PApp : create terminal
P2PApp -> P2PLocalServer : create listening socket
activate P2PLocalServer

...Actions clients...
P2PApp -> P2PRegistrar : stop
activate P2PRegistrar
P2PRegistrar -> P2PKeeper : remove client on port 550
activate P2PKeeper
P2PKeeper --> P2PRegistrar : ok
deactivate P2PKeeper
P2PRegistrar --> P2PApp : ok
deactivate P2PRegistrar
P2PApp -> P2PLocalServer : stop
destroy P2PLocalServer
deactivate P2PApp
@enduml
```

## Diagramme de séquence : demande d'un fichier

```plantuml
@startuml
autonumber
P2PApp -> P2PRegistrar : exists file "a.png"
P2PRegistrar -> P2PKeeper : exists file "a.png"
loop foreach connectedClients
  P2PKeeper -> P2PRemoteClient : do you have file "a.png"
  alt Client has file
    P2PRemoteClient --> P2PKeeper : true
    P2PKeeper -> P2PRemoteClient : which blocks
    alt client1 has
      P2PRemoteClient --> P2PKeeper : file size is 5Mb,  2, 3, 4 and 8
    else client2 has
      P2PRemoteClient --> P2PKeeper : file size is 5Mb,1, 5, 6 and 7
  end
  P2PKeeper --> P2PRegistrar : yes file exists,\n file size is 5Mb, \nblocks client1 has 2, 3, 4 and 8\n client2 has 1,5,6 and 7
  P2PRegistrar --> P2PApp : yes file exists,\n file size is 5Mb, \nblocks client1 has 2, 3, 4 and 8\n client2 has 1,5,6 and 7
  else otherwise
    P2PRemoteClient --> P2PKeeper: false
    P2PKeeper --> P2PRegistrar: false
    P2PRegistrar --> P2PApp: false
end
P2PApp -> P2PLocalFileManager : getBlocPossède "a.png"
activate P2PLocalFileManager
P2PLocalFileManager --> P2PApp : [] (empty)
deactivate P2PLocalFileManager
group client1
  P2PApp -> P2PLocalServer_client1 : connect
  P2PLocalServer_client1 --> P2PApp : OK
  P2PApp --> P2PLocalServer_client1 : envoie block 2, 3, 4 et 8 du fichier "a.png" stp fréro
  loop foreach blocks
    P2PLocalServer_client1 -> P2PLocalFileManager_client1 : read block from a.png
    P2PLocalFileManager_client1 --> P2PLocalServer_client1 : block
    P2PLocalServer_client1 -> P2PApp : block1
    P2PLocalServer_client1 -> P2PApp : 001001110101010... binary data
    P2PApp -> P2PLocalFileManager : write block to a.png
  end
end
group client2
  P2PApp -> P2PLocalServer_client2 : connect
  P2PLocalServer_client2 --> P2PApp : OK
  P2PApp --> P2PLocalServer_client2 : envoie block 1, 5, 6 et 7 du fichier "a.png" stp fréro
  loop foreach blocks
    P2PLocalServer_client2 -> P2PLocalFileManager_client2 : read block from a.png
    P2PLocalFileManager_client2 --> P2PLocalServer_client2 : block
    P2PLocalServer_client2 -> P2PApp : block
    P2PLocalServer_client2 -> P2PApp : 001001110101010... binary data
    P2PApp -> P2PLocalFileManager : write block to a.png
  end
end

@enduml
```

```plantuml
@startuml

package "client" #EEE{
   class P2PApp{}
   class P2PLocalServer{}
   class P2PLocalFileManager{}

}

package "registrar" #EEE {

  class P2PRemoteClient{}
  class P2PKeeper{}
  class P2PRegistrar{}

}
@enduml
```
