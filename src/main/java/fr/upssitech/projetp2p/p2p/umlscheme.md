### uml: class diagram

```plantuml
@startuml
package "Etape 3" #EEE {

  class P2PController {
    - dossier : String
    - tailleBloc : int
    + new(String dossier, int tailleBloc) : P2PController
    + estPresent(String nomFichier) : boolean
    + listeFichier() : List<File>
    + possedeTailleFichier(String fileName) : boolean
    + getTailleFichier(String fileName) : int
    + setTailleFichier(String fileName, int tailleEnOctets)
    + initialiserLesFichiers()
    + blocDuFichier(File fichier, int numeroBloc) : byte[]
    + insererBlocDansFichier(File file, int numeroBloc, byte[] donnee)
    + listeBlocPossedes(File file) : List<Integer>
    + getInfoFichier(File file, String cle)  : String
    + setInfoFichier(File file, String cle, String valeur)
  }

  class P2PApp{
    + main()
  }

  P2PApp --> P2PController

}
@enduml
```
