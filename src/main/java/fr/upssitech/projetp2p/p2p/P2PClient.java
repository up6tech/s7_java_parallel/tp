package fr.upssitech.projetp2p.p2p;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.upssitech.projetp2p.parallel.Utils;

public class P2PClient extends Thread {

  private final P2PController controller;
  private final List<Integer> portsDesAutres;

  public P2PClient(P2PController controller, List<Integer> portsDesAutres) {
    this.controller = controller;
    this.portsDesAutres = portsDesAutres;
  }

  @Override
  public void run() {
    try {
      Scanner entreeClavier = new Scanner(System.in);
      while (true) {
        System.out.println("Commande :");
        String commande = entreeClavier.nextLine();
        if (commande.contains("download")) {
          // Excommande: download doflissou.jpg
          // Ici on veut télécharger un fichier
          String fichier = commande.split("\\s+")[1];
          // Il a déja le fichier
          if (controller.estPresent(fichier)) {
            if(controller.possedeTousLesBlocs(fichier)){
              System.out.println("Vous avez déja ce fichier");
              continue;
            }
          }
          download(fichier);
        } else if (commande.equals("quit")) {
          break;
        } else if (commande.equals("list")) {
          // Ici on affiche l'aide
        } else {
          System.out.println("download - Télécharge un fichier");
          System.out.println("quit - Quitte le logiciel");
        }
      }
      entreeClavier.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void download(String file)
      throws InterruptedException {
    List<Integer> portsDesAutres = new ArrayList<>(this.portsDesAutres);
    AtomicBoolean isDone = new AtomicBoolean();
    while (!isDone.get() || portsDesAutres.size() > 0) {
      int tempsAttente = Utils.nbrRand(5, 10);
      // On mimir pendant un certains temps
      Thread.sleep(tempsAttente);
      // On choisit un serveur random
      int portServeurRandom = Utils.elementAleatoire(portsDesAutres);
      System.out.println("Client: Connexion au serveur sur le port " + portServeurRandom);
      try (Socket socketVersServeur = new Socket("localhost", portServeurRandom)) {
        BufferedReader entreeSocket = new BufferedReader(
            new InputStreamReader(socketVersServeur.getInputStream()));
        PrintStream sortieSocket = new PrintStream(socketVersServeur.getOutputStream());
        // On envoit le nom du fichier au serveur
        sortieSocket.println(file);
        // On lit sa réponse
        String response = entreeSocket.readLine();
        if (response.equals("OK")) {
          int tailleFichier = Integer.parseInt(entreeSocket.readLine());
          controller.setTailleFichier(controller.getDossierDesFichiers() + File.separatorChar + file, tailleFichier);
          // On envoit la liste des blocs qu'on possède
          // Pour ça on converti le List<Integer>
          String blocksChaine = Utils.listToString(controller.listeBlocPossedes(file));
          System.out.println("Client: tiens la liste de blocks que je possède " + blocksChaine);
          sortieSocket.println(blocksChaine);

          // Le serveur nous réponds avec une liste de blocs à télécharger
          response = entreeSocket.readLine();
          System.out.println("Client: le serveur m'envoit " + response);
          if (response.equals("OK")) {
            response = entreeSocket.readLine();
            System.out.println("Client: le serveur m'envoit " + response);
            List<Integer> blocs = Utils.stringToListInts(response);
            System.out.println("Client: j'ai bien reçu la liste des blocs " + blocs);
            for (int numeroBloc : blocs) {
              System.out.println("Client: donne moi le bloc " + numeroBloc);
              // On demande le bloc
              sortieSocket.println(Integer.toString(numeroBloc));
              // d'abord on reçois la taille du bloc
              int byteLength = Integer.valueOf(entreeSocket.readLine());
              System.out.println("Client: la taille du bloc c'est " + byteLength);
              // On télécharge le bloc, donc on passe en mode binaire
              byte[] bloc = new byte[byteLength];
              socketVersServeur.getInputStream().read(bloc);
              // On ajoute le bloc
              controller.insererBlocDansFichier(file, numeroBloc, bloc);
            }
            sortieSocket.println("STOP"); // On a finit de télécharger les blocs
            System.out.println("Client: Les blocs " + blocs + " ont était téléchargé");
            // Est ce qu'on a tous les blocs ??
            socketVersServeur.close();
            isDone.set(controller.possedeTousLesBlocs(controller.getDossierDesFichiers() + File.separator + file));
            if (isDone.get()) {
              System.out.println("Client j'ai tous les blocs");
              System.out.println("Client: j'ai bien téléchargé...");
              break;
            }
            continue;
          } else {
            System.out
                .println("Client: Le serveur localhost:" + portServeurRandom + " ne possède pas les blocs manquants");
            System.out.println("Client: on oublie ce serveur pendant ce téléchargement");
            portsDesAutres.remove((Integer) portServeurRandom);
            socketVersServeur.close();
            continue;
          }
        } else {
          System.out.println("Client: Le serveur localhost:" + portServeurRandom + " ne possède pas le fichier");
          System.out.println("Client: on oublie ce serveur pendant ce téléchargement");
          portsDesAutres.remove((Integer) portServeurRandom);
          socketVersServeur.close();
          continue;
        }

      } catch(ConnectException ce){
        System.out.println("Serveur indisponible localhost:"+Integer.toString(portServeurRandom));
        portsDesAutres.remove((Integer) portServeurRandom);
      }catch (Exception e) {
        e.printStackTrace();
      }
    }
  }


}
