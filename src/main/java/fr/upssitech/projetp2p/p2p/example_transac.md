```plantuml
@startuml
Client -> Serveur: As tu ce fichier ?
Serveur --> Client: Oui j'ai ce fichier fréro
Client -> Serveur: Ok voici mes blocs : 4,8,2,1
Serveur -> Client: ok bah j'ai que le blocs 4 et 8 ntm tiens : 4 et 8
Client -> Serveur: Ok envoie bloc 4
Serveur -> Client: Taille du bloc 4 = 4096 octets
Serveur -> Client: Tiens le bloc en binaire 001010110...
Client -> Serveur: Ok envoie bloc 8
Serveur -> Client: Taille du bloc 8 = 3548 octets
Serveur -> Client: Tiens le bloc en binaire 001011110...
Client -> Serveur: Ok merci c'est bon
Client -> Serveur: aurevoir
Serveur -> Client: aurevoir

@enduml
```
