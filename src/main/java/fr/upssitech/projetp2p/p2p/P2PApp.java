package fr.upssitech.projetp2p.p2p;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class P2PApp {

	public static final int TAILLE_BLOCKS = 4096; // En byte

	/*
	 * Étape 3 : Transformation en P2P simple
	 * Dans cette étape, il n'y a plus de clients ni de serveurs ; les applications
	 * sont les deux à la fois.
	 * Chaque application devra noter de quelle partie du fichier elle dispose. Au
	 * démarrage
	 * certaines applications auront le fichier complet et les autres aucun bloc.
	 * Les applications demanderont aléatoirement chaque bloc manquant à n'importe
	 * quelle autre
	 * application qui renverra soit le bloc soit un message d'erreur
	 */
	public static void main(String[] args) {
		// ex lancement: java -jar P2P -d app1 -p 655 -otherp 284,987,10254
		// -d = le dossier des torrents
		// -p = mon port
		// -otherp = le ports des autres
		List<Integer> portsDesAutres = new ArrayList<>();
		int monPort = -1;
		String dossierOuSontLesTorents = "";

		List<String> arguments = Arrays.asList(args);
		if (arguments.contains("-p")) {
			monPort = Integer.valueOf(arguments.get(arguments.indexOf("-p") + 1));
			// faudrait que je traite le cas ou il mets plusieurs ports en entrée ?
		}
		if (arguments.contains("-otherp")) {
			String listeport = arguments.get(arguments.indexOf("-otherp") + 1);
			Stream.of(listeport.split(",")).mapToInt(Integer::parseInt).forEach(portsDesAutres::add);
		}
		if (arguments.contains("-d")) {
			// dossier
			dossierOuSontLesTorents = arguments.get(arguments.indexOf("-d") + 1);
		}

		// Si les arguments sont manquants il va se faire foutre
		if (monPort == -1 || portsDesAutres.isEmpty() || dossierOuSontLesTorents.equals("")
				|| portsDesAutres.contains(monPort)) {
			System.out.println("Veuilez spécifier les informations suivantes");
			System.out.println(
					"java -jar <JarFile> -d <Dossier> -p <PortDuServeurLocal> -otherp <PortsDesAutresServeurs>");
			System.exit(1);
		}

		P2PController controller = new P2PController(dossierOuSontLesTorents, TAILLE_BLOCKS);
		// TODO On doit init les fichiers qui n'ont pas de metadata
		try {
			controller.initialiserLesFichiers();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final int port = monPort;

		// Externaliser ces deux trucs
		// en deux classes
		new P2PServer(controller, port).start(); // P2PServer
		new P2PClient(controller, portsDesAutres).start(); // P2PClient

		// Ports de l'application
		// Les ports des autres applications
		// Dossier ou sont enregistré les fichiers à donner
		// Mémoriser les fichiers qu'on a
		// Et donc on demande les fichiers qu'on a pas

		// 1 Socket client
		// 1 Socket serveur
		// par machine
		// Une reçois les messages, l'autre en envoie

	}

}
