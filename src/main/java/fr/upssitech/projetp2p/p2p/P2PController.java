package fr.upssitech.projetp2p.p2p;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class P2PController {

    private String dossierDesFichiers;
    private final int tailleBloc;

    // un constructeur
    public P2PController(String dossierDesFichiers, int tailleBloc) {
        this.dossierDesFichiers = dossierDesFichiers;
        this.tailleBloc = tailleBloc;
    }

    /**
     * @return the dossierDesFichiers
     */
    public String getDossierDesFichiers() {
        return dossierDesFichiers;
    }

    /**
     * Vérifie si un fichier est présent dans le dossier
     * 
     * @param nomFichier
     * @return
     */
    public boolean estPresent(String nomFichier) {
        // retourner si le fichier est présent localement

        File userDir = new File(dossierDesFichiers);
        // On obtient la liste des noms des fichiers
        // dans le dossier utilisateur
        String[] fileArray = userDir.list();

        List<String> fileList = Arrays.asList(fileArray);
        return fileList.contains(nomFichier);
    }

    public boolean possedeTousLesBlocs(String nomFichier) throws NumberFormatException, IOException {
        long taille = getTailleFichier(nomFichier);
        int maxBlocsCount = (int) Math.floor(taille / Double.valueOf(this.tailleBloc));
        File file = new File(nomFichier);
        return listeBlocPossedes(file.getName()).size() == maxBlocsCount + 1;
    }

    public File fichier(String nomFichier) {
        if (nomFichier.endsWith(".metaprops"))
            return null;
        return new File(dossierDesFichiers, nomFichier);
    }

    /**
     * Liste des fichiers du dossier
     * 
     * @return
     */
    public List<File> listeFichier() {
        return Arrays.asList(new File(this.dossierDesFichiers).listFiles());
    }

    /**
     * Si on possède le fichier
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public boolean possedeTailleFichier(String fileName) throws IOException {
        String size = getInfoFichier(fileName, "size");
        return size == null;
    }

    /**
     * Récupère la taille du fichier total (Par exemple si on a pas le fichier au
     * complet on aimerai
     * savoir sa taille)
     * 
     * @param fileName
     * @return La taille du fichier en octets
     * @throws NumberFormatException
     * @throws IOException
     */
    public long getTailleFichier(String fileName) throws NumberFormatException, IOException {
        return Long.valueOf(getInfoFichier(fileName, "size"));
    }

    /**
     * Mettre la taille du fichier à jour
     * 
     * @param fileName
     * @param tailleEnOctets
     * @throws IOException
     */
    public void setTailleFichier(String fileName, long tailleEnOctets) throws IOException {
        setInfoFichier(fileName, "size", String.valueOf(tailleEnOctets));
    }

    /**
     * 
     * @throws IOException
     */
    public void initialiserLesFichiers() throws IOException {
        // Regarder tous les fichiers contenu dans le dossier
        // Si leur fichier de metadata n'existe pas
        // On créer le fichier de metadata sur ce fichier, avec 100% des blocs
        // disponibles
        List<File> listfichier = listeFichier();

        for (File fichier : listfichier) {
            if (fichier.getName().endsWith(".metaprops"))
                continue;
            if (!possedeMetadata(fichier.getName())) {
                System.out.println(fichier.getAbsolutePath());
                File metaFile = new File(fichier.getAbsolutePath() + ".metaprops");
                metaFile.createNewFile();
                setTailleFichier(fichier.getAbsolutePath(), fichier.length());
                // On écrit les blocs
                List<Integer> tousLesBlocs = new ArrayList<>();
                // On calcule le nombre de bloc qu'il faut
                int maxBlocsCount = (int) Math.floor(fichier.length() / Double.valueOf(this.tailleBloc));
                for (int numBloc = 0; numBloc <= maxBlocsCount; numBloc++)
                    tousLesBlocs.add(numBloc);
                // Il faut ajouter les blocs dans le fichier de config
                String blockString = String.join(",", tousLesBlocs.stream().map(i -> i.toString()).toList());
                setInfoFichier(fichier.getAbsolutePath(), "blocks", blockString);
            }
        }

    }

    /**
     * Récupérer un certains bloc d'un fichier
     * 
     * @param fichier
     * @param numeroBloc
     * @return
     * @throws IOException
     */
    public byte[] blocDuFichier(File fichier, int numeroBloc) throws IOException {
        // retourne le bloc d'un fichier
        // On ouvre le fichier sous forme de flux de byte
        FileInputStream input = new FileInputStream(fichier);
        // On demande la taille max du fichier
        long totalByteLength = input.getChannel().size();
        int localTailleBloc = tailleBloc;
        // On calcul le décalage que notre curseur va devoir faire
        int offset = localTailleBloc * numeroBloc;
        if (offset > totalByteLength) {
            // On essaye de chercher un bloc qui est en dehors du fichier
            // ex : bloc n°2 de taille de 4Ko et le fichier fait 1 octet
            input.close();
            return new byte[] {};
        }
        // On déplace le curseur de lecture au début du bloc demandé
        input.skipNBytes(offset);
        if (offset + localTailleBloc > totalByteLength) {
            // On essaye de chercher le dernier block, mais la taille du fichier
            // n'est pas un multiple de la taille du block
            // ex : la taille du fichier est de 9Ko, et on découpe par tranche de 2Ko
            localTailleBloc = (int) (totalByteLength - offset);
        }
        byte[] bytes = new byte[localTailleBloc];
        input.read(bytes);
        input.close();
        return bytes;
    }

    /**
     * Méthode pour insérer un bloc dans un fichier
     * 
     * @param file
     * @param numeroBloc
     * @param donnee
     * @throws IOException
     */
    public void insererBlocDansFichier(String file, int numeroBloc, byte[] donnee) throws IOException {
        RandomAccessFile r = new RandomAccessFile(fichier(file), "rw");
        r.seek(numeroBloc * tailleBloc);
        r.write(donnee);
        r.close();

        List<Integer> blocs = listeBlocPossedes(file);
        if (!blocs.contains(numeroBloc)) {
            // On a jamais eu ce bloc, alors on l'ajoute SUUUUUU
            blocs.add(numeroBloc);
        }
        // On transforme notre liste de blocs en chaîne de caractère
        // [1, 2, 3, 5] => "1,2,3,5"
        String blockString = String.join(",", blocs.stream().map(i -> i.toString()).toList());
        setInfoFichier(this.dossierDesFichiers + File.separatorChar + file, "blocks", blockString);
    }

    /**
     * 
     * @param file Le fichier
     * @return La liste des blocs sur notre machine
     * @throws IOException
     */
    public List<Integer> listeBlocPossedes(String file) throws IOException {
        String fichier = fichier(file).getAbsolutePath();

        System.out.println("Local: liste bloc possède fichier : " + fichier);
        try {
            List<Integer> blocks = new ArrayList<>();
            String rawBlocks = getInfoFichier(fichier, "blocks");
            System.out.println("Local: liste des blocs dans fichier de props: '" + rawBlocks + "'");
            if (rawBlocks == null) {
                return blocks;
            }
            String[] blocksStrings = rawBlocks.split("\\,");
            for (String blocksString : blocksStrings) {
                blocks.add(Integer.valueOf(blocksString));
            }
            return blocks;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new ArrayList<>(); // Liste vide
        }
    }

    /**
     * 
     * @param file
     * @param cle
     * @return Null si le fichier n'existe pas Ou si la clé n'existe pas, sinon
     *         renvoie la valeur
     * @throws IOException
     */
    public String getInfoFichier(String file, String cle) throws IOException {
        File fileProperties = new File(file + ".metaprops");
        if (!fileProperties.exists()) {
            fileProperties.createNewFile();
            return null;
        }
        Properties fileProps = new Properties();
        fileProps.load(new FileInputStream(fileProperties));
        return fileProps.getProperty(cle);
    }

    /**
     * 
     * @param file
     * @return
     */
    public boolean possedeMetadata(String file) {
        File metaFile = new File(file + ".metaprops");
        return metaFile.exists();
    }

    /**
     * 
     * @param file
     * @param cle
     * @param valeur
     * @throws IOException Si file est null
     */
    public void setInfoFichier(String file, String cle, String valeur) throws IOException {
        File fileProperties = new File(file + ".metaprops");
        if (!fileProperties.exists()) {
            fileProperties.createNewFile();
        }
        Properties fileProps = new Properties();
        fileProps.load(new FileInputStream(fileProperties));
        fileProps.setProperty(cle, valeur);
        fileProps.store(new FileOutputStream(fileProperties), null);
    }

}
