package fr.upssitech.projetp2p.p2p;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import fr.upssitech.projetp2p.parallel.Utils;

public class P2PGestionClient extends Thread  {
  
  private final Socket socket;
  private P2PServer serveur;

  public P2PGestionClient (P2PServer serveur, Socket socket){
    this.serveur = serveur;
    this.socket = socket;
  }

  public void run() {
    try {
      var controller = this.serveur.getController();
      // méthode traitement d'une connexion
      BufferedReader entreeSocket = new BufferedReader(
          new InputStreamReader(socket.getInputStream()));
      PrintStream sortieSocket = new PrintStream(socket.getOutputStream());

      String file = entreeSocket.readLine();

      System.out.println("Serveur : le fichier demandé est  : " + file);

      if (controller.estPresent(file)) {
        sortieSocket.println("OK");
        Thread.sleep(100);
        sortieSocket
            .println(controller.getTailleFichier(controller.getDossierDesFichiers() + File.separatorChar + file));
      } else {
        System.out.println("Serveur: je n'ai pas le fichier");
        sortieSocket.println("Non je l'ai pas");
        socket.close();
        return;
      }
      String listblock = entreeSocket.readLine();
      List<Integer> blocsDuClients = new ArrayList<>();
      if (!listblock.equals("")) {
        Utils.stringToListInts(listblock);
      } else {
        System.out.println("Serveur: le client ne possède AUCUN blocs");
      }
      List<Integer> blocsDuServeur = controller
          .listeBlocPossedes(file);
      List<Integer> blocsManquants = new ArrayList<>(blocsDuServeur); // On crée
      if (!blocsDuClients.isEmpty()) {
        blocsManquants.removeAll(blocsDuClients);
      }
      if (blocsManquants.isEmpty()) { // Le serveur ne possède pas les blocs manquant du clients
        sortieSocket.println("NO");
        System.out.println("Serveur: Je ne possède pas les blocs " + blocsManquants);
        System.out.println("Serveur: Bloc serveur " + blocsDuServeur);
        System.out.println("Serveur: Bloc client " + blocsDuClients);
        socket.close();
        return;
      } else {
        sortieSocket.println("OK");
        System.out.println("Serveur : j'ai des blocs " + blocsManquants);
        String repListeBlocks = Utils.listToString(blocsManquants);

        sortieSocket.println(repListeBlocks);
        Thread.sleep(100);

        String reponse = entreeSocket.readLine();
        while (repListeBlocks != null && !reponse.equals("STOP")) {
          System.out.println("Serveur : Le bloc demandé est : " + reponse);

          // Ici SUUU
          // taille du bloc puis le contenu bloc
          int numBloc = Integer.parseInt(reponse);

          try {
            byte[] bloc = controller.blocDuFichier(controller.fichier(file), numBloc);
            System.out.println("Serveur : tiens le bloc  " + bloc.length);
            sortieSocket.println(bloc.length);
            Thread.sleep(100);
            socket.getOutputStream().write(bloc);
            socket.getOutputStream().flush();
          } catch (Exception e) {
            e.printStackTrace();
          }

          reponse = entreeSocket.readLine();
          System.out.println("Serveur: suivant s'il vous plait");
        }

        socket.close();
        System.out.println("Serveur: connexion fermée");
        // tant que client n'envoie pas stop
      }
      // utilisation de removeAll
      // blocsDuClients = [1, 4, 5, 7, 9]
      // blocsDuServeur = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
      // blocsManquant = [0, 2, 3, 6, 8]

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
