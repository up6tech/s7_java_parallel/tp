package fr.upssitech.projetp2p.p2p;

import java.net.ServerSocket;
import java.net.Socket;

public class P2PServer extends Thread {

  private final P2PController controller;
  private final int portServeur;

  public P2PServer(final P2PController controller, final int portServeur) {
    this.controller = controller;
    this.portServeur = portServeur;
  }

  @Override
  public void run() {
    while (true) {
      // On créer le serveur qui accepte les sockets TCP
      try (ServerSocket serverSocket = new ServerSocket(portServeur)) {
        // On accepte les sockets
        Socket socket = serverSocket.accept();
        new P2PGestionClient(this, socket).start();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * @return the controller
   */
  public P2PController getController() {
    return controller;
  }

  /**
   * @return the portServeur
   */
  public int getPortServeur() {
    return portServeur;
  }

}
