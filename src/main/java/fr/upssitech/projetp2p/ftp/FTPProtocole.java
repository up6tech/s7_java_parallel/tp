package fr.upssitech.projetp2p.ftp;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FTPProtocole {

  public static final int FTP_PROTOCOL_PORT = 56987;
  private FTPManager ftpManager;

  public FTPProtocole(FTPManager ftpManager) {
    this.ftpManager = ftpManager;
  }

  private int nbrRand(int min, int max) {
    return new Random().nextInt(max - min) + min;
  }

  public String commande(FTPConnection ftpConnection, String commande, final Socket tcpSocket) {
    String[] args = commande.split("\\s+");
    switch (args[0]) {
      case "?":
      case "help":
      case "HELP":
        return FTPManager.HELP_STR;
      case "dwn":
      case "cp":
      case "download":
      case "DWN":
        try {
          String file = args[1];
          // Si l'utilisateur rentre un fichier qui n'est pas présent
          // dans son dossier actuel (relative à la connexion tcp)
          // alors on lui dit fichier inexistant
          if (!ftpManager.contains(ftpConnection, file)) {
            return "ERREUR fichier '" + file + "' inexistant";
          }
          if (ftpManager.isDirectory(ftpConnection, file)) {
            return "ERREUR la cible est un dossier";
          }
          int rPort = nbrRand(1_500, 60_000);
          new Thread(() -> {
            try (var fileSocket = new ServerSocket(rPort)) {
              Socket transfert = fileSocket.accept();
              // Construction d'un PrintStream pour envoyer du texte à travers la connexion
              //
              PrintStream sortieSocket = new PrintStream(transfert.getOutputStream());
              // Open file
              sortieSocket.println(String.valueOf(file));

              byte[] content = Files.readAllBytes(ftpManager
                  .getFile(ftpConnection, file).toPath());
              sortieSocket.println(content.length);

              Thread.sleep(100);

              // Transfert le fichier d'un coup
              transfert.getOutputStream().write(content, 0, content.length);
              transfert.getOutputStream().flush();
              // close transfert
              transfert.close();
              PrintStream sortieSocketPrincipale = new PrintStream(tcpSocket.getOutputStream());
              sortieSocketPrincipale.println("STOP");
            } catch (IOException e) {
              e.printStackTrace();
            } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }).start();
          return "OK OPENDOWNLOAD " + rPort;
        } catch (ArrayIndexOutOfBoundsException e) {
          return "ERREUR le nom du fichier est manquant";
        }
      case "ls":
      case "list":
      case "LIST":
        List<File> files = ftpManager.list(ftpConnection);
        Collections.sort(files, (file1, file2) -> {
          boolean isDir1 = file1.isDirectory();
          boolean isDir2 = file2.isDirectory();
          if (isDir1 && isDir2) {
            return file1.getName().compareTo(file2.getName());
          }
          int ordre = (isDir1 ? 100 : 0) - (isDir2 ? 100 : 0);
          return file1.getName().compareTo(file2.getName()) - ordre;
        });
        var strBuilder = new StringBuilder();
        strBuilder.append("D\t../\n");
        for (var file : files) {
          boolean isDir = file.isDirectory();
          strBuilder
              .append((isDir ? "D" : "F") + "\t" + file.getName() + "/\n");
        }
        strBuilder.replace(strBuilder.length() - 1, strBuilder.length(), "")
            .append("\n");
        return "OK " + strBuilder.toString();
      case "pwd":
      case "PWD":
        return "OK " + ftpConnection.getCurrentPath();
      case "cd":
      case "CD":
        try {
          String dir = args[1];
          if (!ftpManager.contains(ftpConnection, dir)) {
            return "ERREUR dossier '" + dir + "' inexistant";
          }
          if (dir.equals("..")) {
            String currPath = ftpConnection.getCurrentPath();
            int substr = currPath.lastIndexOf(File.separator);
            currPath = currPath.substring(0, substr);
            ftpConnection.setCurrentPath(currPath);
          } else {
            ftpConnection.setCurrentPath(
                ftpConnection.getCurrentPath() + File.separator + dir);
          }
          return "OK";
        } catch (ArrayIndexOutOfBoundsException e) {
          return "ERREUR Argument manquant";
        } catch (Exception e) {
          e.printStackTrace();
          return "ERREUR inconnue";
        }
      default:
        return "ERREUR Commande inconnue";
    }
  }

}
