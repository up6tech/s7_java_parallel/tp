package fr.upssitech.projetp2p.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurFTP {

  public static void main(String[] args) {

    FTPManager ftpManager = new FTPManager();
    FTPProtocole ftpProtocole = new FTPProtocole(ftpManager);

    try (var tcpSocket = new ServerSocket(FTPProtocole.FTP_PROTOCOL_PORT)) {
      System.out.println("Starting FTP server on port "+FTPProtocole.FTP_PROTOCOL_PORT);
      System.out.println("using TCP");
      while (true) {
        var clientSocket = tcpSocket.accept();
        new Thread(() -> {
          try {
            handleClient(clientSocket, ftpProtocole, new FTPConnection());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }).start();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void handleClient(Socket tcpSocket, FTPProtocole ftpProtocole, FTPConnection ftpConnection) throws IOException {
    // Construction d'un BufferedReader pour lire du texte envoyé à travers la
    // connexion socket
    BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
    // Construction d'un PrintStream pour envoyer du texte à travers la connexion
    // socket
    PrintStream sortieSocket = new PrintStream(tcpSocket.getOutputStream());

    String input = "";
    while(input != null){
      input = entreeSocket.readLine();

      if(input != null) {
        System.out.println(tcpSocket.getInetAddress() + " -> SERVER : " + input);
        String result = ftpProtocole.commande(ftpConnection, input, tcpSocket) + '\u001a';
        System.out.println("SERVER -> " + tcpSocket.getInetAddress() + " : " + result);
        sortieSocket.println(result);
      }
    }

    tcpSocket.close();
  }

}
