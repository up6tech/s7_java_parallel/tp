package fr.upssitech.projetp2p.ftp;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FTPManager {

  
  public static final String HELP_STR =
    """
      Voici la liste des commandes disponible sur ce serveur:
      LIST,list,ls    - Voir la list des fichiers ET dossiers 
      PWD             - Affiche le dossier dans le quel vous vous trouvez
      CD, cd          - Vous déplace dans un dossier spécifique
      DWN,download,cp - Télécharge un fichier vers votre machine 

    """;
    

  /**
   * Renvoie la liste des fichiers contenu dans le répertoire de l'utilisateur
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @return La liste des fichiers
   */
  public List<File> list(FTPConnection ftpConnection) {
    File file = new File(ftpConnection.getCurrentPath());
    return Arrays.asList(file.listFiles());
  }

  /**
   * Vérifie si un dossier est contenu dans le répertoire de l'utilisateur courrant
   * @param ftpConnection Donnée de l'utilisateur (connexion tcp)
   * @param dir dossier à vérifier
   * @return True si le dossier est contenu, False sinon
   */
  public boolean contains(FTPConnection ftpConnection, String dir) {
    if(dir.equals("..")) return true;
    File userDir = new File(ftpConnection.getCurrentPath());
    // On obtient la liste des noms de fichier / dossiers 
    // dans le dossier utilisateur
    String[] fileArray = userDir.list();
    // On passe du tableau à l'objet
    /*
    Le code en dessous est équivalent à ce truc (en C)
    for (int i = 0; i < fileArray.length; i++) {
      if(fileArray[i].equals(dir)){
        return true;
      }
    }
    return false;
    */

    List<String> fileList = Arrays.asList(fileArray);
    return fileList.contains(dir);
  }

  /**
   * Transforme une chaîne de caractère en objet File de java
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @param file le nom du fichier
   * @return L'objet fichier
   */
  public File getFile(FTPConnection ftpConnection, String file){
    return new File(ftpConnection.getCurrentPath()+File.separator+file);
  }

  /**
   * Vérifie si le nom entrée en paramètre est un dossier
   * @param ftpConnection Les données de l'utilisateur (connexion tcp)
   * @param file le nom du fichier
   * @return L'objet fichier
   */
  public boolean isDirectory(FTPConnection ftpConnection, String file) {
    return getFile(ftpConnection, file).isDirectory();
  }
}
