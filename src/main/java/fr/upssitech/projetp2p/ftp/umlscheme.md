# UML Partie 1 : FTP


## Diagramme de cas d'utilisation
```plantuml
@startuml
left to right direction
actor Utilisateur as s
package FTP{
  usecase "Télécharger un fichier" as UC1
  usecase "Se déplacer dans un dossier" as UC2
  usecase "Afficher la liste des fichiers" as UC3
}
s --> UC1
s --> UC2
s --> UC3
@enduml
```

## Diagramme de séquence : se déplacer dans un fichier
```plantuml
@startuml
autonumber
ClientFTP -> ServeurFTP: "Connexion TCP"
ServeurFTP --> ClientFTP: "Accept"
ClientFTP -> ServeurFTP: "CD download"
ServeurFTP -> ServeurFTP: handleClient(ftpConnection)
loop while socket.isConnected()
  ServeurFTP -> FTPProtocole: commande(ftpConnection, commande)
  FTPProtocole -> FTPConnection: setCurrentDir("download")
  FTPProtocole --> ServeurFTP: "OK CD download"
  ServeurFTP --> ClientFTP: "OK CD download"
end
@enduml
```

## Diagramme de séquence : afficher la liste des fichiers
```plantuml
@startuml
ClientFTP -> ServeurFTP: "Connexion TCP"
ServeurFTP --> ClientFTP: "Accept"
ClientFTP -> ServeurFTP: "LS"
ServeurFTP -> ServeurFTP: handleClient
ServeurFTP -> FTPProtocole: commande(ftpConnection, "LS")
FTPProtocole -> FTPManager: listFile(ftpConnection)
FTPManager --> FTPProtocole: List<File> files
FTPProtocole -> FTPProtocole: sort(files)
FTPProtocole -> FTPProtocole: toString(files)
FTPProtocole --> ServeurFTP: ..\nD\ttest\nF\tfile.txt
ServeurFTP --> ClientFTP: ..\nD\ttest\nF\tfile.txt
@enduml
```

## Diagramme de séquence : téléchargement
```plantuml
@startuml
autonumber
ClientFTP -> ServeurFTP: "Connexion TCP"
ServeurFTP --> ClientFTP: "Accept"
ClientFTP -> ServeurFTP: "DOWNLOAD file.txt"
ServeurFTP -> ServeurFTP: handleClient(ftpConnection)
loop while socket.isConnected()
  ServeurFTP -> FTPProtocole: commande(ftpConnection, "DOWNLOAD file.txt")
  FTPProtocole -> FTPManager: fileExists(ftpConnection, "file.txt")
  FTPManager --> FTPProtocole: true
  FTPProtocole -> FTPManager: isDirectory(ftpConnection, "file.txt")
  FTPManager --> FTPProtocole: false
  FTPProtocole -> Thread: createDownloadThread
  FTPProtocole --> ServeurFTP: "OK OPENDOWNLOAD 45000"
  note right
    Envoie le numéro du port du socket de téléchargement
  end note
  ServeurFTP --> ClientFTP: "OK OPENDOWNLOAD 45000"
  ClientFTP -> Thread: "Connexion TCP 45000"
  Thread --> ClientFTP: "Accept"
  Thread -> ClientFTP: "file.txt"
  note right
    Renvoie le nom du fichier
  end note
  Thread -> FTPManager: fileSize(ftpConnection, "file.txt")
  FTPManager --> Thread: 548
  Thread -> ClientFTP: 548
  note right
    Renvoie la taille en byte du fichier
  end note
  Thread -> FTPManager: fileContent(ftpConnection, "file.txt")
  FTPManager --> Thread: filecontent
  Thread -> ClientFTP: "01010111011..."
  note right
    Envoie le fichier sous <b>forme binaire</b> au client
  end note
  Thread -> ClientFTP: socket.close()
  ClientFTP -> Thread: socket.close()
end
@enduml
```


## Diagramme de classe
```plantuml
@startuml
package "ClassDiagram" #EEE {

    class ServeurFTP{
      + main()
      + handleClient(Socket soc, FTPProtocole prot, FTPConnection con)
    }

    class ClientFTP {
      + main()
    }

    class FTPManager {
      + List<File> list(FTPConnection con)
      + boolean contains(FTPConnection con, String dir)
      + File getFile(FTPConnection con, String file)
      + boolean isDirectory(FTPConnection con, String file)
    }

    class FTPConnection{
      - dossierActuel : String
      + getDossierActuel() : String
      + setDossierActuel(String dossier)
    }

    class FTPProtocole{
      + String commande(con connection, String command, Socket soc)
    }

    FTPProtocole --> FTPManager
    ServeurFTP --> FTPConnection
    ServeurFTP --> FTPProtocole


}
@enduml
```
