package fr.upssitech.projetp2p.ftp;

public class FTPConnection {

  /**
   * Dossier dans le quel l'utilisateur se trouve
   */
  private String currentPath;

  public FTPConnection(){
    this.currentPath = System.getProperty("user.dir");
  }

  public String getCurrentPath() {
    return currentPath;
  }

  public void setCurrentPath(String currentPath) {
    this.currentPath = currentPath;
  }

}
