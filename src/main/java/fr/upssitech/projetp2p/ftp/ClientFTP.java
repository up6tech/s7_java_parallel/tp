package fr.upssitech.projetp2p.ftp;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class ClientFTP {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    var quitFunc = List.of("q", "quit", "exit");

    try (Socket tcpClient = new Socket("localhost", FTPProtocole.FTP_PROTOCOL_PORT)) {

      // Construction d'un BufferedReader pour lire du texte envoyé à travers la
      // connexion socket
      BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpClient.getInputStream()));
      // Construction d'un PrintStream pour envoyer du texte à travers la connexion
      // socket
      PrintStream sortieSocket = new PrintStream(tcpClient.getOutputStream());

      String chaine = "";
      System.out.println("Tapez vos phrases ou '" + String.join(",", quitFunc) + "' pour arrêter :");

      while (true) {
        // lecture clavier
        System.out.print("<- : ");

        chaine = scanner.nextLine();
        if (quitFunc.contains(chaine.toLowerCase())) {
          System.out.println("FIN programme");
          break;
        }
        sortieSocket.println(chaine); // on envoie la chaine au serveur

        // lecture d'une chaine envoyée à travers la connexion socket
        InputLoop: while (true) {
          chaine = entreeSocket.readLine();
          if (chaine == null || chaine.equals("STOP")) {
            break InputLoop;
          }
          if (chaine.startsWith("OK OPENDOWNLOAD ")) {
            final int transfertPort = Integer.parseInt(
                chaine.substring("OK OPENDOWNLOAD ".length()).replaceAll(String.valueOf('\u001a'), ""));
            System.out.println("\t-> Downloading file from port " + transfertPort);
            // Open a transfert thread
            new Thread(() -> {
              try {
                Thread.sleep(50);
              } catch (Exception e) {
              }
              try (Socket transfertSocket = new Socket("localhost", transfertPort)) {

                // Construction d'un BufferedReader pour lire du texte envoyé à travers la
                // connexion socket
                BufferedReader transfertBufferReader = new BufferedReader(
                    new InputStreamReader(transfertSocket.getInputStream()));

                String fileName = transfertBufferReader.readLine();
                int fileLength = Integer.parseInt(transfertBufferReader.readLine());

                System.out.println("\t-> File '" + fileName + "' (" + fileLength + " bytes) is downloading...");

                byte[] fileByte = transfertSocket.getInputStream().readNBytes(fileLength);

                try (FileOutputStream fileWriter = new FileOutputStream(fileName)) {
                  fileWriter.write(fileByte);
                  System.out.println("\t-> File '" + fileName + "' downloaded.");
                  transfertSocket.close();
                } catch (Exception e) {
                  e.printStackTrace();
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }).start();
            continue;
          }
          if (chaine.endsWith(String.valueOf('\u001a'))) {
            System.out.println("-> : " + chaine);
            break;
          } else {
            System.out.println(chaine);
          }
        }
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
    scanner.close();
  }
}
