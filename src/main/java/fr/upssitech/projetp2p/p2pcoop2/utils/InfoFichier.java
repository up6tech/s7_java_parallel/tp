package fr.upssitech.projetp2p.p2pcoop2.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.upssitech.projetp2p.p2pcoop2.client.P2PApp;

public class InfoFichier {

  public static final InfoFichier VIDE = new InfoFichier("none", -1, Map.of());

  public static InfoFichier deserial(String string) throws Exception {
    String[] data = string.split("/");
    String fileName = data[0];
    long fileSize = Long.valueOf(data[1]);
    String blocksData = data[2];

    List<Integer> blocks = new ArrayList<>();
    for (String blockData : blocksData.split("\\*")) {
      blocks.add(Integer.valueOf(blockData));
    }
    return new InfoFichier(fileName, fileSize, blocks);
  }

  private String fileName;
  private long fileSize;
  private Map<Integer, byte[]> blocks;

  /**
   * Constructeur pour la transmission,
   * ici on ne rentre pas le contenu du fichier, mais seuleument des données sur
   * celui ci
   * 
   * @param fileName Nom du fichier
   * @param fileSize Taille du fichier
   * @param blocks   Les numéros des blocs
   */
  public InfoFichier(String fileName, long fileSize, List<Integer> blocks) {
    this.fileName = fileName;
    this.fileSize = fileSize;
    this.blocks = new HashMap<>();
    for (int numeroBloc : blocks) {
      this.blocks.put(numeroBloc, new byte[] {});
    }
  }

  /**
   * Constructeur représentant toutes les données
   * 
   * @param fileName Nom du fichier
   * @param fileSize Taille du fichier
   * @param blocks   Les numéros des blocs ainsi que leur contenu
   */
  public InfoFichier(String fileName, long fileSize, Map<Integer, byte[]> blocks) {
    this.fileName = fileName;
    this.fileSize = fileSize;
    this.blocks = blocks;
  }

  /**
   * @return the fileName
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * @return the blocks
   */
  public Map<Integer, byte[]> getBlocks() {
    return blocks;
  }

  /**
   * 
   * @return
   */
  public List<Integer> getListeBlocs() {
    return new ArrayList<>(this.blocks.keySet());
  }

  /**
   * 
   * @param numeroBloc Le numéro du bloc
   * @return Les données binaire, sinon NULL si le numéro du bloc est incorrect
   */
  public byte[] getBloc(int numeroBloc) {
    return this.blocks.get(numeroBloc);
  }

  /**
   * @return the fileSize
   */
  public long getFileSize() {
    return fileSize;
  }

  public String serial() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.fileName + "/");
    builder.append(this.fileSize + "/");
    builder.append(String.join("*", getListeBlocs().stream().map(i -> i.toString()).toList()));
    return builder.toString();
  }

  public long averageByteTransmitted(){
    return getListeBlocs().size() * P2PApp.BLOCK_SIZE - (P2PApp.BLOCK_SIZE / 2);
  }

  @Override
  public String toString() {
    return "{" +
        " fileName='" + getFileName() + "'" +
        ", fileSize='" + getFileSize() + "'" +
        ", blocks='" + getListeBlocs() + "'" +
        "}";
  }

  

}
