package fr.upssitech.projetp2p.p2pcoop2.utils;

public record Pair<U, V>(U first, V second) {}