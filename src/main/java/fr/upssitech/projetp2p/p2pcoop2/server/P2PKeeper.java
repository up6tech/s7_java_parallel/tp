package fr.upssitech.projetp2p.p2pcoop2.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.upssitech.projetp2p.p2pcoop2.utils.InfoFichier;
import fr.upssitech.projetp2p.p2pcoop2.utils.InfoFichierTransfert;
import fr.upssitech.projetp2p.p2pcoop2.client.P2PApp;
import fr.upssitech.projetp2p.p2pcoop2.client.P2PLocalFileManager;

public class P2PKeeper {

  private List<P2PRemoteClient> connectedClients = new ArrayList<>();

  public boolean isAvalaible(int port) {
    for (P2PRemoteClient client : connectedClients) {
      if (client.getPort() == port)
        return false;
    }
    return true;
    /*
     * return !connectedClients
     * .stream()
     * .anyMatch(p2pClient -> p2pClient.getPort() == port);
     */
  }

  /**
   * @return the connectedClients
   */
  public List<P2PRemoteClient> getConnectedClients() {
    return connectedClients;
  }

  public InfoFichierTransfert infoTransfert(String fileName, int portAsking) {
    long taille = 0;
    // La structure de donnée "Set" n'autorise pas de doublons
    Map<Integer, List<Integer>> blocsByClient = new HashMap<>();
    for (P2PRemoteClient remoteClient : connectedClients) {
      if (remoteClient.getPort() == portAsking) {
        // On ne va pas voir le client qui demande un fichier
        continue;
      }
      // Pour chaque client connecté au registrar
      try {
        System.out.println("[Registrar] Demande info fichier client sur le port " + remoteClient.getPort());
        InfoFichier infoFichier = remoteClient.possedeLeFichier(fileName);
        if (infoFichier != InfoFichier.VIDE) {
          if (taille == 0) { // Si on a pas encore trouvé un client qui a des infos
            taille = infoFichier.getFileSize();
          }
          // Ajout la liste des blocs possèdé par ce client
          // dans la liste des blocs à transmettre
          System.out.println("[Registrar] Info : " + infoFichier);
          blocsByClient.put(remoteClient.getPort(), infoFichier.getListeBlocs());
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (taille == 0 || blocsByClient.isEmpty()) {
      // Ici, aucun client n'a le fichier
      System.out.println("[Registrar] Aucune infos sur le fichiers '" + fileName + "' (pour "
          + this.connectedClients.size() + " clients)");
      return InfoFichierTransfert.VIDE;
    }
    InfoFichierTransfert infoFichier = new InfoFichierTransfert(fileName, taille, blocsByClient);
    System.out.println("[Registrar] Le fichier a été récupéré : " + infoFichier);
    return infoFichier;
  }

  /**
   * 
   * @param fileName
   * @return InfoFichier
   */
  public InfoFichier infoFichier(String fileName) {
    long taille = 0;
    // La structure de donnée "Set" n'autorise pas de doublons
    Set<Integer> blocs = new HashSet<>();
    // https://gitlab.com/up6tech/s7_java_parallel/tp/-/blob/main/src/main/java/fr/upssitech/projetp2p/p2pcoop/umldiagrams.md
    for (P2PRemoteClient remoteClient : connectedClients) {
      // Pour chaque client connecté au registrar
      try {
        System.out.println("[Registrar] Demande info fichier client sur le port " + remoteClient.getPort());
        InfoFichier infoFichier = remoteClient.possedeLeFichier(fileName);
        if (infoFichier != InfoFichier.VIDE) {
          if (taille == 0) { // Si on a pas encore trouvé un client qui a des infos
            taille = infoFichier.getFileSize();
          }
          // Ajout la liste des blocs possèdé par ce client
          // dans la liste des blocs à transmettre
          System.out.println("[Registrar] Info : " + infoFichier);
          blocs.addAll(infoFichier.getListeBlocs());

          // On calcule le nombre moyen de byte transmis par le client
          long averageByteTransmitted = infoFichier.averageByteTransmitted();
          remoteClient.setByteSent(remoteClient.getByteSent()+averageByteTransmitted);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (taille == 0 || blocs.isEmpty()) {
      // Ici, aucun client n'a le fichier
      System.out.println("[Registrar] Aucune infos sur le fichiers '" + fileName + "' (pour "
          + this.connectedClients.size() + " clients)");
      return InfoFichier.VIDE;
    }
    InfoFichier infoFichier = new InfoFichier(fileName, taille, new ArrayList<>(blocs));
    System.out.println("[Registrar] Le fichier a été récupéré : " + infoFichier);
    return infoFichier;
  }

  public void addClient(P2PRemoteClient client) {
    System.out.println("[Registrar] Client enregistré sur le port " + client.getPort());
    this.connectedClients.add(client);
  }

  public void removeClient(P2PRemoteClient client) {
    System.out.println("[Registrar] Client déconnecté sous le port " + client.getPort());
    this.connectedClients.remove(client);
  }

}