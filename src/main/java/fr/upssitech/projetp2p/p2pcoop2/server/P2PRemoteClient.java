package fr.upssitech.projetp2p.p2pcoop2.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Objects;

import fr.upssitech.projetp2p.p2pcoop2.utils.InfoFichier;
import fr.upssitech.projetp2p.p2pcoop2.utils.InfoFichierTransfert;
import fr.upssitech.projetp2p.parallel.Utils;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class P2PRemoteClient extends Thread {


  public static P2PRemoteClient dummyObject(){
    return new P2PRemoteClient();
  }

  private final P2PRegistrar registrar;
  private Socket openedSocket;
  private int port;
  private long byteSent;
  private long byteReceived;

  public P2PRemoteClient(P2PRegistrar p2pRegistrar, Socket openedSocket, int port) {
    this.registrar = p2pRegistrar;
    this.openedSocket = openedSocket;
    this.port = port;
    this.byteSent = 0;
    this.byteReceived = 0;
  }

  public P2PRemoteClient (){
    this.registrar = null;
  }

  /**
   * @return the byteSent
   */
  public long getByteSent() {
    return byteSent;
  }

  /**
   * @param byteSent the byteSent to set
   */
  public void setByteSent(long byteSent) {
    this.byteSent = byteSent;
  }

  /**
   * @return the byteReceived
   */
  public long getByteReceived() {
    return byteReceived;
  }

  /**
   * @param byteReceived the byteReceived to set
   */
  public void setByteReceived(long byteReceived) {
    this.byteReceived = byteReceived;
  }

  /**
   * @return the openedSocket
   */
  public Socket getOpenedSocket() {
    return openedSocket;
  }

  @Override
  public void run() {
    try {
      // Ici on peut lire la socket et tout
      BufferedReader lecture = new BufferedReader(new InputStreamReader(this.openedSocket.getInputStream()));
      PrintStream ecriture = new PrintStream(this.openedSocket.getOutputStream());

      String line = lecture.readLine();
      // Le client démarre bien la connexion par register
      if (line.startsWith("register")) {
        System.out.println("[Registrar] Recherche d'un port... ");
        // On génère un port aléatoirement non utilisé
        int randomPort = 0;
        do {
          randomPort = Utils.nbrRand(2000, 9999);
          // TODO peut être vérifier le nombre d'itération pour pas resté bloqué dans
        } while (!this.registrar.getKeeper().isAvalaible(randomPort));

        this.port = randomPort;
        this.registrar.getKeeper().addClient(this);

        ecriture.println("OK " + randomPort);
      } else {
        // Le début de la connexion ne correspond pas à "register XXXXX"
        // TODO : le faire dégager par exemple
      }

      // now wait for command
      while (true) {

        String inputCommand = lecture.readLine();
        if (inputCommand.startsWith("exists file ")) {
          // Ask for file existence
          String askedFile = inputCommand.substring("exists file ".length());
          InfoFichierTransfert infoFichier = this.registrar.getKeeper().infoTransfert(askedFile, this.port);

          if (infoFichier == InfoFichierTransfert.VIDE) {
            // Si le fichier est inexistant, il faut lui dire
            ecriture.println("no");
          } else if (this.registrar.canDownload(this)) {
            // TODO faire en sorte d'annuler les demandes si jamais il ne fait pas assez
            // d'envoie
          } else {
            try {
              ecriture.println(infoFichier.serial());
              System.out.println("[RemoteClient] Envoie du fichier '" + infoFichier + "'");
              this.setByteReceived(this.getByteReceived() + infoFichier.averageByteTransmitted());
            } catch (Exception e) {
              e.printStackTrace(System.err);
              System.err.println("[RemoteClient] erreur lecture du fichier");
            }
          }
        } else if (inputCommand.equals("stop")) {
          this.registrar.getKeeper().removeClient(this);
          ecriture.print("ok");
          break; // Sortir de la boucle
        }
      }
      // Si on est ici alors on doit close
      this.openedSocket.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @return the port
   */
  public int getPort() {
    return port;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (!(o instanceof P2PRemoteClient)) {
      return false;
    }
    P2PRemoteClient p2PClient = (P2PRemoteClient) o;
    return port == p2PClient.port;
  }

  @Override
  public int hashCode() {
    return Objects.hash(port);
  }

  /**
   * Renvoie toute les informations sur le fichier que possède le client
   * 
   * @param fileName le nom du fichier
   * @return InfoFichier.VIDE si le client n'a pas le fichier, Sinon, les
   *         informations sur le fichier (nom, taille, blocs qu'il possède)
   * @throws IOException S'il y eu une erreur
   */
  public InfoFichier possedeLeFichier(String fileName) throws IOException {
    try (Socket socket = new Socket(InetAddress.getByName("localhost"), this.port)) {
      System.out.println("[Registrar] Demande des info fichiers ...");
      // Ici on peut lire la socket et tout
      BufferedReader lecture = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      PrintStream ecriture = new PrintStream(socket.getOutputStream());

      ecriture.println("registrar");
      ecriture.println("exists file " + fileName);
      String response = lecture.readLine();
      // Demander si le client à le fichier
      if (response.equals("no")) {
        System.out.println("[Registrar] Pas d'info");
        socket.close();
        return InfoFichier.VIDE;
      } else {
        System.out.println("[Registrar] Information récup");
        socket.close();
        return InfoFichier.deserial(response);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return InfoFichier.VIDE;
  }

}
