package fr.upssitech.projetp2p.p2pcoop2.server;

import java.net.ServerSocket;
import java.net.Socket;

import fr.upssitech.projetp2p.p2pcoop2.client.P2PApp;

public class P2PRegistrar implements Runnable {

  public static final int REGISTRAR_PORT = 54789;
  public static final long DOWNLOAD_THRESHOLD = 5 * P2PApp.BLOCK_SIZE;

  public static void main(String[] args) {
    new P2PRegistrar()
        .run();
  }

  private P2PKeeper keeper;

  public P2PRegistrar() {
    this.keeper = new P2PKeeper();
  }

  /**
   * @return the keeper
   */
  public P2PKeeper getKeeper() {
    return keeper;
  }

  @Override
  public void run() {
    System.out.println("[Registrar] Démarrage du registrar sur le port " + REGISTRAR_PORT);
    try (ServerSocket registrarSocket = new ServerSocket(REGISTRAR_PORT)) {

      while (true) {
        // un client se connect est demande l'enregistrement
        Socket socket = registrarSocket.accept();
        System.out.println("[Registrar] Reception client ");
        P2PRemoteClient client = new P2PRemoteClient(this, socket, 0);
        client.start();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public boolean canDownload(P2PRemoteClient client) {
    // Le nombre d'octet envoyé par le client passé en paramètre
    long nbOctetsEnvoye = client.getByteSent();

    // Le nombre d'octet reçu par le client passé en paramètre
    long nbOctetsRecu = client.getByteReceived();

    // La différence entre le nombre d'octet envoyé et le nombre d'octet reçu
    long delta = Math.abs(nbOctetsEnvoye - nbOctetsRecu);

    return delta < DOWNLOAD_THRESHOLD;
  }

}
