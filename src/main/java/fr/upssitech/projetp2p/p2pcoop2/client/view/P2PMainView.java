package fr.upssitech.projetp2p.p2pcoop2.client.view;

import javax.swing.JFrame;
import javax.swing.JTextField;

import fr.upssitech.projetp2p.p2pcoop2.client.P2PApp;

public class P2PMainView {

  private P2PApp app;
  private JFrame mainFrame;

  public P2PMainView(P2PApp app) {
    this.app = app;
    this.mainFrame = new JFrame("Application P2P - STRI");
    mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    mainFrame.setSize(1280, 720);
    buildComponent();
    mainFrame.setLocationRelativeTo(null);
    mainFrame.setVisible(true);
  }

  public void buildComponent(){
    JTextField dirText = new JTextField(app.getFileManager().getFolder());
    dirText.setEditable(false);
    mainFrame.add(dirText);
  }

  /**
   * 
   */
  public void syncFiles(){
    
  }

}
