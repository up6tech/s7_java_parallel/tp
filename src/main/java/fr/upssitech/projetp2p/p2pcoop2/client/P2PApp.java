package fr.upssitech.projetp2p.p2pcoop2.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import fr.upssitech.projetp2p.p2pcoop2.client.view.P2PMainView;
import fr.upssitech.projetp2p.p2pcoop2.server.P2PRegistrar;
import fr.upssitech.projetp2p.p2pcoop2.utils.InfoFichierTransfert;
import lombok.Getter;

public class P2PApp extends Thread {

  public static final int BLOCK_SIZE = 4096;
  private P2PLocalServer localServer;
  private P2PLocalFileManager fileManager;
  @Getter
  private P2PState p2pState;
  @Getter
  private Socket socketRegistrar;
  @Getter
  private P2PMainView mainFrame;

  public P2PApp(int listenPort, Socket socketRegistrar, String dir) {
    try {
      this.fileManager = new P2PLocalFileManager(BLOCK_SIZE, dir);
      this.p2pState = new P2PState();
      this.fileManager.initialiserLesFichiers();
      this.localServer = new P2PLocalServer(listenPort, fileManager);
      this.socketRegistrar = socketRegistrar;
      this.mainFrame = new P2PMainView(this);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    List<String> argList = Arrays.asList(args);

    // On récupère l'Ip quand l'utilisateur lance le processus
    String ip = "localhost";
    String dir = "app";

    if (argList.indexOf("-d") == -1) {
      System.out.println("(pas de dossier passé en paramètre)");
    } else {
      dir = argList.get(argList.indexOf("-d") + 1);
    }

    if (argList.indexOf("-i") == -1) {
      System.out.println("(pas d'ip passé en paramètre)");
    } else {
      ip = argList.get(argList.indexOf("-i") + 1);
    }

    System.out.println("[Client] Utilisation de l'ip '" + ip
        + "' pour le registrar");
    System.out.println("[Client] Utilisation du dossier '" + dir
        + "' pour le client");
    // Aide : Ip par un string = InetAddress.getByName(String host)
    // IPv4 ou bien même nom machine

    // Indice pour le port du registrar:
    // Connection au registrar
    int p2pRegistrarPort = P2PRegistrar.REGISTRAR_PORT;
    System.out.println("[Client] Lancement de l'application...");
    init(ip, p2pRegistrarPort, dir);
  }

  /**
   * Initialise les sockets d'écoute du serveur local du client
   * 
   * @param ip
   * @param p2pRegistrarPort
   */
  private static void init(String ip, int p2pRegistrarPort, String dir) {
    try {
      Socket socketRegistrar = new Socket(InetAddress.getByName(ip), p2pRegistrarPort);
      BufferedReader lecture = new BufferedReader(new InputStreamReader(socketRegistrar.getInputStream()));
      PrintStream ecriture = new PrintStream(socketRegistrar.getOutputStream());

      // envoyer le message "register"
      ecriture.println("register");
      System.out.println("[Client] Demande du port au registrar...");
      // Attendre la réception du message
      String reponse = lecture.readLine();

      // format de la réponse "OK %d" avec %d = le port
      String[] portAsString = reponse.split(" ");
      // Si OK :
      if (reponse.startsWith("OK ") && portAsString.length == 2) {
        System.out.println("[Client] OK!");
        // - extraire le port
        // On récupère le port (donc 2eme élèment du tableau)
        try {
          int port = Integer.parseInt(portAsString[1]);
          System.out.println("[Client] Port alloué : " + port);
          // Démarré l'app P2P
          new P2PApp(port, socketRegistrar, dir).start();
        } catch (NumberFormatException e) {
          // on a reçu "OK ??" et ?? n'est pas un int
          System.err.println("ERR: Mauvais retour du serveur, attendu : un port, reçu : " + reponse);
          socketRegistrar.close();
          System.exit(1);
        }
      } else {
        System.err.println("ERR: Mauvais retour du serveur, attendu : 'OK <Port>', reçu : " + reponse);
        socketRegistrar.close();
        System.exit(1);
      }

      // Sinon :
      // Fermer l'application en prévenant le client
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void run() {
    // On démarre le serveur d'écoute
    this.localServer.start();
    // Ici le client hurle
    try {
      Scanner entreeClavier = new Scanner(System.in);
      while (true) {
        System.out.println("[Client] Commande :");
        String commande = entreeClavier.nextLine();
        if (commande.startsWith("download")) {
          // Excommande: download doflissou.jpg
          // Ici on veut télécharger un fichier
          String fichier = commande.split("\\s+")[1];
          // Il a déja le fichier
          if (this.fileManager.existe(fichier)) {
            System.out.println("Vous avez déja ce fichier");
            continue;
          }
          download(fichier);
        } else if (commande.equals("quit")) {
          break;
        } else {
          System.out.println("download - Télécharge un fichier");
          System.out.println("quit - Quitte le logiciel");
        }
      }
      entreeClavier.close();
      quit();
      this.localServer.interrupt();
      return;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void download(String fichier) throws Exception {
    try {
      BufferedReader lecture = new BufferedReader(new InputStreamReader(socketRegistrar.getInputStream()));
      PrintStream ecriture = new PrintStream(socketRegistrar.getOutputStream());

      ecriture.println("exists file " + fichier);

      String response = lecture.readLine();
      if (response.equals("no")) {
        // le fichier existe pas (ratio)
        System.out.println("[Client] le fichier '" + fichier + "' n'existe pas");
      } else {
        System.out.println("[Client] le fichier '" + fichier + "' existe");
        InfoFichierTransfert infoFichier = InfoFichierTransfert.deserial(response);
        System.out.println("[Client] Info : " + infoFichier);

        for (var portEtBlocs : infoFichier.getBlocsByClient().entrySet()) {
          int port = portEtBlocs.getKey();
          List<Integer> blocks = portEtBlocs.getValue();

          try (Socket downloadBlocSocket = new Socket("localhost", port)) {

            BufferedReader lectureDownload = new BufferedReader(
                new InputStreamReader(downloadBlocSocket.getInputStream()));
            PrintStream ecritureDownload = new PrintStream(downloadBlocSocket.getOutputStream());

            System.out.println("[Client] Demande des blocs " + blocks + " sur le port " + port);

            // On demande en mode download
            ecritureDownload.println("download");
            // On envoit le fichier demandé
            ecritureDownload.println(fichier);
            // On envoit le nombre de blocks demandé
            ecritureDownload.println(Integer.toString(blocks.size()));
            // On envoit le bloc demandé, on attends la taille du bloc en byte puis on
            // attends la réponse binaire
            for (int bloc : blocks) {
              System.out.println("[Client] Demande du bloc " + bloc);
              ecritureDownload.println(Integer.toString(bloc));

              int byteLength = Integer.parseInt(lectureDownload.readLine());
              System.out.println("[Client] Taille du bloc : " + byteLength);

              byte[] blocData = new byte[byteLength];
              downloadBlocSocket.getInputStream().read(blocData);
              System.out.println("[Client] Réception du bloc");
              this.fileManager.insererBlocDansFichier(fichier, bloc, blocData);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }

      // Attendre le retour

    } catch (IOException exception) {
      exception.printStackTrace();
    }

  }

  public void quit() throws IOException {

    BufferedReader lecture = new BufferedReader(new InputStreamReader(socketRegistrar.getInputStream()));
    PrintStream ecriture = new PrintStream(socketRegistrar.getOutputStream());
    ecriture.println("stop");
    lecture.readLine();
    this.socketRegistrar.close();
  }

  /**
   * @return the localServer
   */
  public P2PLocalServer getLocalServer() {
    return localServer;
  }

  /**
   * @return the fileManager
   */
  public P2PLocalFileManager getFileManager() {
    return fileManager;
  }
}
