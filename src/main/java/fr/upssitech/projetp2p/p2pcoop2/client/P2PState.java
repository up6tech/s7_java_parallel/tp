package fr.upssitech.projetp2p.p2pcoop2.client;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class P2PState {

  private boolean acceptDownload;

  public P2PState(){
    this.acceptDownload = true;
  }

}
