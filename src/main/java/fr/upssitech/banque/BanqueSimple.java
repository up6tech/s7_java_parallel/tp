package fr.upssitech.banque;

import java.util.Date;
import java.util.HashMap;

public class BanqueSimple {
	class CompteEnBanque {
		private double solde;
		private Date derniereOperation;

		public CompteEnBanque(double solde) {
			this.solde = solde;
			derniereOperation = new Date(); // recupere la date courante
		}

		public double getSolde() {
			return solde;
		}

		public Date getDerniereOperation() {
			return derniereOperation;
		}

		public void ajouter(double somme) {
			solde += somme;
			derniereOperation = new Date(); // recupere la date courante
		}

		public void retirer(double somme) {
			solde -= somme;
			derniereOperation = new Date(); // recupere la date courante
		}
	}

	HashMap<String, CompteEnBanque> comptes;

	public BanqueSimple() {
		comptes = new HashMap<String, CompteEnBanque>();
	}

	public void creerCompte(String id, double somme) {
		comptes.put(id, new CompteEnBanque(somme));
	}
	
	public boolean compteExiste(String id) {
		return comptes.containsKey(id);
	}

	public void ajouter(String id, double somme) {
		CompteEnBanque cpt = comptes.get(id);
		cpt.ajouter(somme);
	}

	public void retirer(String id, double somme) {
		CompteEnBanque cpt = comptes.get(id);
		cpt.retirer(somme);
	}

	public double getSolde(String id) {
		CompteEnBanque cpt = comptes.get(id);
		return cpt.getSolde();
	}

	public Date getDerniereOperation(String id) {
		CompteEnBanque cpt = comptes.get(id);
		return cpt.getDerniereOperation();
	}

	public void clear(){
		this.comptes.clear();
	}

	public static void main(String[] args) {
    
	}

}
