package fr.upssitech.banque;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class ClientTCP {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    var quitFunc = List.of("q", "quit", "exit");

    try (Socket tcpClient = new Socket("localhost", 40_000)) {

      // Construction d'un BufferedReader pour lire du texte envoyé à travers la
      // connexion socket
      BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpClient.getInputStream()));
      // Construction d'un PrintStream pour envoyer du texte à travers la connexion
      // socket
      PrintStream sortieSocket = new PrintStream(tcpClient.getOutputStream());

      String chaine = "";
      System.out.println("Tapez vos phrases ou '" + String.join(",", quitFunc) + "' pour arrêter :");

      while (true) {
        // lecture clavier
        System.out.print("<- : ");
        chaine = scanner.nextLine();
        if (quitFunc.contains(chaine.toLowerCase())) {
          System.out.println("FIN programme");
          break;
        }
        sortieSocket.println(chaine); // on envoie la chaine au serveur

        // lecture d'une chaine envoyée à travers la connexion socket
        chaine = entreeSocket.readLine();
        System.out.println("-> : " + chaine);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
    scanner.close();
  }

}
