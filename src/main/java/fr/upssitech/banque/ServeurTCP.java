package fr.upssitech.banque;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTCP {

  public static void main(String[] args) {

    BanqueSimple banqueSimple = new BanqueSimple();
    final GestionProtocole gestionProtocole = new GestionProtocole(banqueSimple);

    System.out.println("STARTING SERVER IN TCP");

    try (ServerSocket tcpServer = new ServerSocket(40_000)) {
      while (true) {
        Socket tcpSocket = tcpServer.accept();
        Thread tcpThread = new Thread(() -> {
          try {
            startClient(tcpSocket, gestionProtocole);
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
        tcpThread.start();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void startClient(Socket tcpSocket, GestionProtocole gestionProtocole) throws IOException {

    // Construction d'un BufferedReader pour lire du texte envoyé à travers la
    // connexion socket
    BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
    // Construction d'un PrintStream pour envoyer du texte à travers la connexion
    // socket
    PrintStream sortieSocket = new PrintStream(tcpSocket.getOutputStream());

    String command = "";
    while (command != null) {
      command = entreeSocket.readLine();

      if (command != null) {
        System.out.println(tcpSocket.getInetAddress() + " -> SERVER : " + command);
        String result = gestionProtocole.traiter(command);
        System.out.println("SERVER -> " + tcpSocket.getInetAddress() + " : " + result);
        sortieSocket.println(result);
      }
    }
    if (tcpSocket.isClosed()) {
      tcpSocket.close();
    }
  }

}
