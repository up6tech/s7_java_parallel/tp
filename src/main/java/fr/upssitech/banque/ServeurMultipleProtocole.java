package fr.upssitech.banque;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurMultipleProtocole {

    public static void main(String[] args) {

        BanqueSimple banqueSimple = new BanqueSimple();
        final GestionProtocole gestionProtocole = new GestionProtocole(banqueSimple);
        final int port = 40_000;

        System.out.println("STARTING SERVER WITH MULTIPLE PROTOCOL");

        Thread tcpThread = new Thread(() -> tcpRunnable(port, gestionProtocole));
        Thread udpThread = new Thread(() -> updRunnable(port, gestionProtocole));
        tcpThread.setName("thread-tcp");
        udpThread.setName("thread-udp");

        tcpThread.start();
        udpThread.start();
    }

    public static void tcpRunnable(int port, GestionProtocole gestionProtocole) {
        try (ServerSocket tcpServer = new ServerSocket(40_000)) {
            while (true) {
                Socket tcpSocket = tcpServer.accept();
                Thread tcpThread = new Thread(() -> {
                    try {
                        startTCPClient(tcpSocket, gestionProtocole);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                tcpThread.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startTCPClient(Socket tcpSocket, GestionProtocole gestionProtocole) throws IOException {

        // Construction d'un BufferedReader pour lire du texte envoyé à travers la
        // connexion socket
        BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
        // Construction d'un PrintStream pour envoyer du texte à travers la connexion
        // socket
        PrintStream sortieSocket = new PrintStream(tcpSocket.getOutputStream());

        String command = "";
        while (command != null) {
            command = entreeSocket.readLine();

            if (command != null) {
                System.out.println(tcpSocket.getInetAddress() + " -> TCP SERVER : " + command);
                String result = gestionProtocole.traiter(command);
                System.out.println("TCP SERVER -> " + tcpSocket.getInetAddress() + " : " + result);
                sortieSocket.println(result);
            }
        }
        if (tcpSocket.isClosed()) {
            tcpSocket.close();
        }
    }

    public static void updRunnable(int port, GestionProtocole gestionProtocole) {
        try (// Création d'un socket UDP sur le port 40000
                DatagramSocket socket = new DatagramSocket(40000)) {
            // tampon pour recevoir les données des datagrammes UDP
            final byte[] tampon = new byte[1024];

            // objet Java permettant de recevoir un datagramme UDP
            DatagramPacket dgram = new DatagramPacket(tampon, tampon.length);

            while (true) {
                // attente et réception d'un datagramme UDP
                socket.receive(dgram);

                // extraction des données
                String chaine = new String(dgram.getData(), 0, dgram.getLength());

                System.out.println(dgram.getAddress() + " -> UDP SERVER : " + chaine);

                String result = gestionProtocole.traiter(chaine);

                System.out.println("UDP SERVER -> " + dgram.getAddress() + " : " + result);

                // On converti le résultat (chaîne) en tableau de bytes
                dgram.setData(result.getBytes());
                dgram.setLength(result.length());
                // on renvoie le message au client
                socket.send(dgram);

                // on replace la taille du tampon au max
                // elle a été modifiée lors de la réception
                dgram.setData(tampon);
                dgram.setLength(tampon.length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
