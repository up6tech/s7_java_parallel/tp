package fr.upssitech.banque;

import java.util.Date;

public class GestionProtocole {

  private BanqueSimple banqueSimple;

  public GestionProtocole(BanqueSimple banqueSimple) {
    this.banqueSimple = banqueSimple;
  }

  public String traiter(String commande) {
    // On coupe la chaîne en suivant les espaces
    // En gros ce regex c'est : on cut à 1 espace ou plus
    String[] args = commande.split("\\s+");
    if (args.length < 2)
      // Impossible qu'une commande contienne moins de 1 argument
      return "ERREUR Commande incomplète";
    String cmd = args[0];
    try {
      // L'id du compte est commun à toutes les commandes
      String id = args[1];
      switch (cmd) {
        // CREATION id montantInitial
        case "CREATION":
          double sommeInitiale = Double.parseDouble(args[2]);
          if (this.banqueSimple.compteExiste(id))
            // Il y a déja un compte avec le même identifiant
            return "ERREUR Compte existant";
          // On doit créer le compte avec le montant initial
          this.banqueSimple.creerCompte(id, sommeInitiale);
          return "OK CREATION";
        case "POSITION":
          if (!this.banqueSimple.compteExiste(id)) {
            // On demande un compte qui n'existe pas
            return "ERREUR Compte inexistant";
          }
          // On récupère le solde actuel du compte
          double solde = this.banqueSimple.getSolde(id);
          Date dateDerniereOperation = this.banqueSimple
              .getDerniereOperation(id);
          return "POS " + solde + " " + dateDerniereOperation.toString();
        case "AJOUT":
          if (!this.banqueSimple.compteExiste(id)) {
            // On demande un compte qui n'existe pas
            return "ERREUR Compte inexistant";
          }
          double ajout = Double.valueOf(args[2]);
          if (ajout <= 0) {
            return "ERREUR Impossible d'ajouter un montant négatif";
          }
          this.banqueSimple.ajouter(id, ajout);
          return "OK AJOUT";
        case "RETRAIT":
          if (!this.banqueSimple.compteExiste(id)) {
            // On demande un compte qui n'existe pas
            return "ERREUR Compte inexistant";
          }
          double retrait = Double.valueOf(args[2]);
          if (retrait <= 0) {
            return "ERREUR Impossible de retirer un montant négatif";
          }
          this.banqueSimple.retirer(id, retrait);
          return "OK RETRAIT";
        default:
          return "ERREUR Commande inconnue";
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      return "ERREUR Commande incomplète";
    }

  }
}
