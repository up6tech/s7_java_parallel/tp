package fr.upssitech.banque;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServeurUDP {

  public static void main(String[] args) throws Exception {

    BanqueSimple banqueSimple = new BanqueSimple();
    final GestionProtocole gestionProtocole = new GestionProtocole(banqueSimple);

    System.out.println("STARTING SERVER IN UDP");

    try (// Création d'un socket UDP sur le port 40000
        DatagramSocket socket = new DatagramSocket(40000)) {
      // tampon pour recevoir les données des datagrammes UDP
      final byte[] tampon = new byte[1024];

      // objet Java permettant de recevoir un datagramme UDP
      DatagramPacket dgram = new DatagramPacket(tampon, tampon.length);

      while (true) {
        // attente et réception d'un datagramme UDP
        socket.receive(dgram);

        // extraction des données
        String chaine = new String(dgram.getData(), 0, dgram.getLength());

        System.out.println(dgram.getAddress() + " -> SERVER : " + chaine);

        String result = gestionProtocole.traiter(chaine);

        System.out.println("SERVER -> "+dgram.getAddress()+" : "+result);
      
        // On converti le résultat (chaîne) en tableau de bytes
        dgram.setData(result.getBytes());
        dgram.setLength(result.length());
        // on renvoie le message au client
        socket.send(dgram);

        // on replace la taille du tampon au max
        // elle a été modifiée lors de la réception
        dgram.setData(tampon);
        dgram.setLength(tampon.length);
      }
    }

  }

}
