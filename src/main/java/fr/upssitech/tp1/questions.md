# TD / TP numéro 1

## Question 1 :

### Comment doit-­on faire pour éviter que les affichages des threads soient mélangés ?

```
On doit synchroniser l'objet de sortie de texte sur la console dans les deux classes
```

### 2.1. Quel problème soulève l’interaction Production/Consommation sur l’objet unique « file de message ».Comment le résoudre en java ?

```
2 thread différents vont intéragir sur le même objet en même temps (possiblement), car ce ne sont pas des opérations atomiques
```

```
Pour le résoudre en java, on pourrait synchroniser l'accès à l'objet dans les deux classes comme ceci


synchronized (listeMessage){
    ...
}
```

### 2.2. Que signifie « thread-­safe » ? Vector est-­il « thread-­safe » ? Est-­ce que ça résout le problème de synchronisation ?

```
L'appelation Thread-Safe fait réfèrence au fait que l'objet peut être utilisé par plusieurs thread simultanément sans cassé son fonctionnement et sa véracité

D'après la JavaDoc du JDK 16 de Vector (https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/Vector.html)

Chaque accès est synchronisé, il est donc "Thread-Safe"

Oui puisque le Vector peut être utilisé à la place d'une ArrayList lorsqu'il s'agit d'opérations réparties sur plusieurs Thread, il faut donc remplacer le type du champ "message" dans la classe Producteur vers "java.util.Vector<String>"

```

### Piste d'amélioration

```
On utilise la liste comme une file, on aurait pu utiliser une autre classe du JDK comme ConcurrentModificationQueue pour utiliser des méthodes comme peek() et pop(), pour sortir le haut de la file, cette implémentation de Queue supporte les accès concurrents (donc Thread-Safe)
```
