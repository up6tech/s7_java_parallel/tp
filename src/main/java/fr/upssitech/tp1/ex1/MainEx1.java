package fr.upssitech.tp1.ex1;

public class MainEx1 {

    public static void main(String[] args) {
        ObjetA a = new ObjetA();
        ObjetB b = new ObjetB();

        new Thread(a).start();
        b.start();
    }

}
