package fr.upssitech.tp1.ex1;

public class ObjetB extends Thread {

    @Override
    public void run() {
        // On synchronise sur l'objet nous permettant d'output sur la console
        while (true) {
            synchronized (System.out) {
                for (int i = 2; i <= 6; i++)
                    System.out.println(i + "ème ligne du thread B");
            }
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
