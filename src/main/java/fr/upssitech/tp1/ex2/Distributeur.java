package fr.upssitech.tp1.ex2;

public class Distributeur {

    public static void main(String[] args) {
        int nombreDeFile = 1,
            nombreDeProducteur = 5,
            nombreDeConsommateur = 25;


        Producteur producteur = new Producteur();
        Consommateur consommateur = new Consommateur(producteur);
        // On lance les threads
        producteur.start();
        consommateur.start();
    }

}
