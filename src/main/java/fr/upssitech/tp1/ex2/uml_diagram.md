### uml: class diagram
```plantuml
@startuml
package "Exercice 1" #FFF {
    
    class Producteur << Thread >>{
        # {static} MAXFILE : int = 5
        # {static} MAXMESSAGEs : int = 50
        - messages : int
        - nbMessage : int
        + run()
        - insererMessage() : String
        + recupererMessage() : String
    }

    class Consommateur << Thread >>{
        # {static} MAXMESSAGES : int = 50;
        - nbMessage : int
        ~ producteur : Producteur
        + run()


    }

    Consommateur "1..1" - Producteur

}
@enduml
```