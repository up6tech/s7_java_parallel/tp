package fr.upssitech.tp1.ex2;

import java.util.Vector;

public class ProducteurV2 {

  public static class File {

    private Vector<String> messages = new Vector<>();

    private final int taille;
    private int numero;

    public File(int numero, int taille) {
      this.numero = numero;
      this.taille = taille;
    }

    public void produire() {
      // On force le thread a rentrer dans une boucle
      // tant que la file ne se vide pas
      while (messages.size() == this.taille) {
        System.out.println("File n°"+this.numero+" pleine");
      }
      // On ajoute un message correspondant à la date
      messages.add(new java.util.Date().toString());
    }

    public void consommer(){

    }

  }

  public static class Producteur implements Runnable{

    private static final int MAX_MESSAGE = 50;

    private File file;
    private int nbMessage = 0;
    private int numProducteur;
    
    public Producteur (int numProducteur, File file){
      this.file = file;
      this.numProducteur = numProducteur;
    }

    @Override
    public void run() {
      try {
        while (nbMessage < MAX_MESSAGE) {
            // Tant que le nombre de messages max n'a pas été atteint, on continu à en
            // produire et en ajouter dans la liste
            this.file.produire();
            System.out.println("Message numero " + nbMessage + " produit");
            nbMessage++;
            // On fait dormir le thread pendant 1 seconde
            Thread.sleep(1000);
        }
        System.out.println("Je stop produire la fréro (producteur n°"+this.numProducteur+")");
    } catch (InterruptedException e) {}
      
    }

  }

}
