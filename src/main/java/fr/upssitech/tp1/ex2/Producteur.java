package fr.upssitech.tp1.ex2;

import java.util.Vector;

class Producteur extends Thread {
    static final int MAXFILE = 5;
    static final int MAXMESSAGES = 50;
    // OLD = private ArrayList<String> messages;
    private Vector<String> messages;
    private int nbMessage;

    public Producteur() {
        // Initialisation des champs
        // OLD = messages = new ArrayList<String>();
        messages = new Vector<>();
        nbMessage = 0;
    }

    public void run() {
        try {
            while (nbMessage < MAXMESSAGES) {
                // Tant que le nombre de messages max n'a pas été atteint, on continu à en
                // produire et en ajouter dans la liste
                insererMessage();
                System.out.println("Message numero " + nbMessage + " produit");
                nbMessage++;
                // On fait dormir le thread pendant 1 seconde
                sleep(1000);
            }
        } catch (InterruptedException e) {
        }
    }

    private void insererMessage() {
        // On force le thread a rentrer dans une boucle
        // tant que la file ne se vide pas
        while (messages.size() == MAXFILE) {
            System.out.println("File pleine");
        }
        // On ajoute un message correspondant à la date
        messages.add(new java.util.Date().toString());
    }

    public String recupererMessage() {
        // Tant que la file est vide, on attend un message
        while (messages.size() == 0) {
            System.out.println("File vide");
        }
        // On récupère le message
        String message = (String) messages.get(0);
        // On supprime le message
        messages.remove(0);
        return message;
    }
}

class Consommateur extends Thread {
    static final int MAXMESSAGES = 50;
    private int nbMessage;

    Producteur producteur;

    Consommateur(Producteur p) {
        producteur = p;
        nbMessage++;
    }

    public void run() {
        try {
            while (nbMessage < MAXMESSAGES) {
                String message = producteur.recupererMessage();
                System.out.println("Message numero " + nbMessage + " recu : " + message);
                nbMessage++;
                sleep(2000);
            }
        } catch (InterruptedException e) {
        }
    }
}
