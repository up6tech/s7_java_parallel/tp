package fr.upssitech;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import fr.upssitech.banque.BanqueSimple;
import fr.upssitech.banque.GestionProtocole;

@TestInstance(Lifecycle.PER_CLASS)
public class GestionProtocoleTest {

  private GestionProtocole gestionProtocole;
  private BanqueSimple banqueSimple;

  @BeforeAll
  public void init() {
    this.banqueSimple = new BanqueSimple();
    this.gestionProtocole = new GestionProtocole(banqueSimple);
  }

  @AfterEach
  public void afterEach() {
    this.banqueSimple.clear();
  }

  @Test
  public void testCreationCompte() {
    String cmd = "CREATION ACC001 50";
    String currDate = new Date().toString();
    assertEquals("OK CREATION", this.gestionProtocole.traiter(cmd));
    cmd = "POSITION ACC001";
    assertEquals("POS 50.0 " + currDate, this.gestionProtocole.traiter(cmd));
  }

  @Test
  public void testDoubleCreationCompte() {
    String cmd = "CREATION ACC001 50";
    assertEquals("OK CREATION", this.gestionProtocole.traiter(cmd));
    assertEquals("ERREUR Compte existant", this.gestionProtocole.traiter(cmd));
  }

  @Test
  public void testFileSeparator() {
    String operating = System.getProperty("os.name");
    if (operating.contains("win")) {
      // Windows case
      assertEquals("\\", File.separator);
    } else if (operating.contains("nix")) {
      assertEquals("/", File.separator);
    }
  }

  @Test
  public void testParsingByteSize() {
    String EOF = String.valueOf('\u001a');
    String incomingCommand = "OK 580 " + EOF;

    String byteCountAsString = incomingCommand;
    // Reçu = return "OK " + nbByte
    int byteSize = Integer
        .parseInt(
            byteCountAsString.substring(3)
                .replace(EOF, "")
                .trim());

    assertEquals(580, byteSize);
  }

}
