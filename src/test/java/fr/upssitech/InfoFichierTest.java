package fr.upssitech;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichier;

public class InfoFichierTest {

  @Test
  public void onSerial() {
    try {
      InfoFichier info = new InfoFichier("a.png", 69420, List.of(1, 2, 3, 4, 5));
      String data = info.serial();
      InfoFichier info2 = InfoFichier.deserial(data);
      assertEquals(info.getFileName(), info2.getFileName());
      assertEquals(info.getFileSize(), info2.getFileSize());
      assertEquals(info.getListeBlocs().get(0), info2.getListeBlocs().get(0));
    } catch (Exception e) {
    }
  }

}
