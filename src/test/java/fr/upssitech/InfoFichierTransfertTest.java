package fr.upssitech;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import fr.upssitech.projetp2p.p2pcoop.utils.InfoFichierTransfert;

public class InfoFichierTransfertTest {
  

  @Test
  public void onSerial() {
    try {
      Map<Integer, List<Integer>> blocByClient = new HashMap<>();
      // Clé :  port, Valeurs: liste des blocs
      blocByClient.put(5895, List.of(1, 2, 3));
      blocByClient.put(5896, List.of(4, 5, 6));
      blocByClient.put(5899, List.of(7, 8, 9));

      InfoFichierTransfert info = new InfoFichierTransfert("a.png", 69420, blocByClient);
      String data = info.serial();
      InfoFichierTransfert info2 = InfoFichierTransfert.deserial(data);

      assertEquals(info.getFileName(), info2.getFileName());
      assertEquals(info.getFileSize(), info2.getFileSize());
      
      assertTrue(blocByClient.containsKey(5895));
      List<Integer> blocs = blocByClient.get(5895);
      assertEquals(1, blocs.get(0));
      assertEquals(2, blocs.get(1));
      assertEquals(3, blocs.get(2));

      assertTrue(blocByClient.containsKey(5896));
      blocs = blocByClient.get(5896);
      assertEquals(4, blocs.get(0));
      assertEquals(5, blocs.get(1));
      assertEquals(6, blocs.get(2));

      assertTrue(blocByClient.containsKey(5899));
      blocs = blocByClient.get(5899);
      assertEquals(7, blocs.get(0));
      assertEquals(8, blocs.get(1));
      assertEquals(9, blocs.get(2));
      
    } catch (Exception e) {
    }
  }

}
