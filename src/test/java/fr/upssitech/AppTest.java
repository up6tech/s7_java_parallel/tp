package fr.upssitech;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.upssitech.projetp2p.p2pcoop2.server.P2PRegistrar;
import fr.upssitech.projetp2p.p2pcoop2.server.P2PRemoteClient;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    public static final int BLOCK_SIZE = 4096;
    private static P2PRegistrar registrar;

    @BeforeAll
    public static void before(){
        registrar = new P2PRegistrar();
    }

    @Test
    public void testDownloadFalse(){
        var dummyClient = new P2PRemoteClient();
        dummyClient.setByteSent(1024);
        dummyClient.setByteReceived(20*BLOCK_SIZE);
        assertFalse(registrar.canDownload(dummyClient));
    }

    @Test
    public void testDownloadTrue(){
        var dummyClient = new P2PRemoteClient();
        dummyClient.setByteSent(1024);
        dummyClient.setByteReceived(1024);
        assertTrue(registrar.canDownload(dummyClient));
    }
}
