package fr.upssitech;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.upssitech.projetp2p.parallel.Utils;

public class TestParallelDownload {

    public static void main(String[] args) {

        List<Integer> serverPorts = List.of(56789, 57450, 58945);
        int totalAmountOfBloc = 20;

        Map<Integer, List<Integer>> portServeurVersBlocs = new HashMap<>();

        for (int numeroBloc = 0; numeroBloc <= totalAmountOfBloc; numeroBloc++) {
            int numeroServeur = Utils.nbrRand(0, serverPorts.size());
            int portServeur = serverPorts.get(numeroServeur);
            if (portServeurVersBlocs.containsKey(portServeur)) {
                portServeurVersBlocs.get(portServeur).add(numeroBloc);
            } else {
                List<Integer> listeDesBlocs = new ArrayList<>();
                listeDesBlocs.add(numeroBloc);
                portServeurVersBlocs.put(portServeur, listeDesBlocs);
            }
        }

        System.out.println(portServeurVersBlocs);

    }

}
