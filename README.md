# tp

Repository de TP / TD de la matière Java concept de parallèlisation du semestre 7 d'UPSSITECH - STRI

Requiert une install VSCode + Java16 + Extension Markdown + Extension PlantUML pour tout voir

## Visualisation des diagrammes sur GitLab:

[Etape 1](src/main/java/fr/upssitech/projetp2p/ftp/umlscheme.md)

[Etape 2](src/main/java/fr/upssitech/projetp2p/parallel/umlscheme.md)

[Etape 3](src/main/java/fr/upssitech/projetp2p/p2p/umlscheme.md)

[Etape 4](src/main/java/fr/upssitech/projetp2p/p2pcoop/umldiagrams.md)

[Etape 4](src/main/java/fr/upssitech/projetp2p/p2pcoop2/umldiagrams.md)